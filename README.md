## FS1030 database + React EMR system

Team: Samira, Brenda, Gareth, Jannatul

2020-3 to 2020-4

run this app:
open one terminal:

```
npm start
```

open another terminal at same root folder:

```
npm run dev
```

**File structure description:(written by Brenda)**

- Worked on basic project code structure (frontend + backend) to build a foundation to start.
- Managing Trello.
- Admin CRUD (patient List and Provider list folders) Frontend + Backend.
- Under Routes folder emrRouter.js is the entry to different routes.
- ProviderAdd.js – admin add care provider
- providerList.js- list all care providers
- providerUpdate.js – admin update care provider
- patient.js – admin add care patient
- patientList.js- list all patients
- patientUpdate.js – admin update a patient
- search patient and search care provider feature, Frontend + Backend.
- PatientSearch.js - Admin search a patient
- ProviderSearch.js- admin search a care provider
- first version of patient profile page + sidebar (Frontend + Backend) with notes component frontend layout.
- Components-> PatientProfle folder -> MainPagePatientProfile, MainPagePatientNote
- Components->  Siderbar folder
- First version of UI/ UX design by using material UI.
- Backend Error catching by middleware.
- Create sql files. database/emr_careProvider.sql, emr_patient_profile.sql.

**File structure description:(written by Gareth)**

- Worked on Patient Testing, Patient Radiology and Patient Billing Routes and well as Testing List/Create/Update components, Billing List/Create/Update Components and Radiology List/Create/Update components.
- Data base schema for Radiology, Billing and Testing can be found in the database folder in the src folder in our app. Radiology is the emr_patient_radiology.sql file, testing is the emr_patient_labTest.sql file and billing is the emr_patient_billing.sql file.
- Under the patientRadiologyRoute.js file SELECT SQL select statements are used at line 10 and 35, INSERT at 67, UPDATE at 100 and DELETE at 125 and these are applied respectively in the Radiology add/list/update components.
- Under the patientTestingRoute.js file SELECT SQL select statements are used at line 10 and 33, INSERT at 65, UPDATE at 100 and DELETE at 125 and these are applied respectively in the Testing add/list/update components.
- Under the patientBillingRoute.js file SELECT SQL select statements are used at line 10 and 33, INSERT at 66, UPDATE at 102 and DELETE at 127 and these are applied respectively in the Billing add/list/update components.

**File structure description:(written by Jannatul)**

- Worked on Patients note Routes and testing notes Add/ Update/ Create and Delete components.
- emrRouter.js -Included routes for Patient Note. PatientNoteRouter.js -- CRUD Operation Patient Note
- PatineNoteAdd.js – Add Patient Note PatientNoteUpdate.js- Update Patient Note
- Data base schema for Patient note can be found in the database folder in the src folder in our app named emr_patient_notes.sql file

**File structure description:(written by Samira)**
I am going to keep it short and sweet:
- Sign up
- Login
- Authentication/Authorization
- Medical History CRUD (includes the schema, backend and front-end logic)
- Immunization CRUD (includes the schema, backend and front-end logic)
- Allergy CRUD (includes the schema, backend and front-end logic)
- Improved overall logic and look and feel of the application
- Maintainer of the repo (Test and fix any bug issue to keep the sanity of our code)