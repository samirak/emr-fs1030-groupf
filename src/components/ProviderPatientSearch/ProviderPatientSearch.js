import React, { useState, useEffect } from "react";
import axios from "axios";
import { StyledContainer, StyledFormArea } from "../ProviderList/style.js";
import "../ProviderList/style.css";
import {
  TableBody,
  TableCell,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TextField
} from "@material-ui/core";
import { Link } from "react-router-dom";
import Paper from "@material-ui/core/Paper";


function ProviderPatientSearch() {
  const [patient, setPatient] = useState([]);
  const [healthCard, setHealthCard] = useState("");
  const URL = "http://localhost:5500/api/admin/patient";
  // const history = useHistory();

  // get data from DB
  useEffect(() => {
    async function fetchData() {
      try {
        let {
          data: { data },
        } = await axios.get(`${URL}`, {
          headers: {
            "Content-Type": "application/json",
          },
        });
        console.log(Array.from(data));

        let search = Array.from(data).filter(
          (item) => item.patient_healthcard === healthCard
        );
        if (search) {
          setPatient(search);
        }
      } catch (err) {
        console.log(err);
      }
    }
    fetchData();
  }, [healthCard]);

  return (
    <div className="provider-search-list">
      <StyledContainer style={{ position: "relative" }}>
        <StyledFormArea
          style={{ backgroundColor: "#fff", borderRadius: "10px" }}
        >
          <div>
            <h1>Patient Search</h1>            

            <form className="register_provider_form">
              <TextField
                style={{ marginBottom: "30px" }}
                id="standard-full-width"
                required
                placeholder="111-111-1111"
                label="Input Health Card 10 Number"
                variant="outlined"
                type="text"
                fullWidth
                InputLabelProps={{
                  shrink: true,
                }}
                value={healthCard}
                onChange={(e) => setHealthCard(e.target.value)}
                color="primary"
              />
            </form>

            {patient ? (
              patient.map((p) => (
                <div>
                  <h2>Result</h2>
                  <TableContainer component={Paper}>
                    <Table>
                      <TableHead>
                        <TableRow>
                          <TableCell
                            align="center"
                            style={{ fontWeight: "700", color: "#230051" }}
                          >
                            Health Card Number
                          </TableCell>
                          <TableCell
                            align="center"
                            style={{ fontWeight: "700", color: "#230051" }}
                          >
                            Patient Name
                          </TableCell>
                          <TableCell
                            align="center"
                            style={{ fontWeight: "700", color: "#230051" }}
                          >
                            Date Of Birth <br></br>(yyyy-mm-dd)
                          </TableCell>
                          <TableCell
                            align="center"
                            style={{ fontWeight: "700", color: "#230051" }}
                          >
                            Patient Phone
                          </TableCell>
                          <TableCell
                            align="center"
                            style={{ fontWeight: "700", color: "#230051" }}
                          >
                            Patient Address
                          </TableCell>
                          <TableCell
                            align="center"
                            style={{ fontWeight: "700", color: "#230051" }}
                          >
                            Gender
                          </TableCell>
                          <TableCell
                            align="center"
                            style={{ fontWeight: "700", color: "#230051" }}
                          >
                            Patient Email
                          </TableCell>
                          <TableCell
                            align="center"
                            style={{ fontWeight: "700", color: "#230051" }}
                          >
                            Marital
                          </TableCell>
                          <TableCell
                            align="center"
                            style={{ fontWeight: "700", color: "#230051" }}
                          >
                            Insurance Number
                          </TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        <TableRow key={p.paitient_healthcard}>
                          <TableCell align="center">
                            <Link
                              to={`/api/admin/patientList/${p.patient_healthcard}`}
                            >
                              {p.patient_healthcard}
                              <h3 className="details">Go To Details</h3>
                            </Link>
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_firstname} {p.patient_lastname}
                          </TableCell>
                          <TableCell align="center">{p.patient_DOB}</TableCell>
                          <TableCell align="center">
                            {p.patient_phone}
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_street},{p.patient_city},
                            {p.patient_province},{p.patient_postalcode}
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_gender}
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_email}
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_marital}
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_insurance}
                          </TableCell>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </TableContainer>
                </div>
              ))
            ) : (
              <div>Cannot find it.</div>
            )}
          </div>
        </StyledFormArea>
      </StyledContainer>
    </div>
  );
}

export default ProviderPatientSearch;
