import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import {
  InputLabel,
  MenuItem,
  FormControl,
  TextField,
  Button,
  Select,
  makeStyles,
} from "@material-ui/core";
import "../Allergy/style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: 30,
  }
}));

export default function MedicalHistoryUpdate(props) {
  const classes = useStyles();
  const [name, setName] = useState("");
  const [condition, setCondition] = useState("");
  const [dose, setDose] = useState("");
  const [description, setDescription] = useState("");
  const [datePrescribed, setDatePrescribed] = useState(
    new Date("2021-03-18T21:11:54")
  );
  const [provider, setProvider] = useState("");
  const [success, setSuccess] = useState(false);
  const [medical, setMedical] = useState({});
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/medical";

  useEffect(() => {
    const id = props.match.params.id;

    axios.get(`${URL}/record/${id}`).then(({ data }) => {
      setMedical(data.result[0]);
      console.log(data.result[0]);
    });
  }, [props.match.params.id]);

  const healthID = medical.patient_healthcard;

  const submitUpdateMedical = async (event, id) => {
    event.preventDefault();
    let updateMedical = {
      medical_history_medication: name,
      medical_history_condition: condition,
      medical_history_dose: dose,
      medical_history_date_prescribed: datePrescribed,
      medical_history_provider: provider,
      medical_history_decs: description,
    };

    try {
      axios.put(`${URL}/update/${id}`, updateMedical).then((res) => {
        console.log("Successfully updated medical history record!");
        setMedical(updateMedical);
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/medical/list/${healthID}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Update medical history</h1>
      <form onSubmit={(e, id) => submitUpdateMedical(e, props.match.params.id)}>
        <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder={medical.patient_healthcard}
          label="Health Card Number"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={medical.patient_healthcard}
        />
        <TextField
          className={classes.textField}
          id="medicine-name"
          placeholder={medical.medical_history_medication}
          label="Medicine Name"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <TextField
          className={classes.textField}
          id="condition-name"
          placeholder={medical.medical_history_condition}
          label="Condition"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={condition}
          onChange={(e) => setCondition(e.target.value)}
        />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="medicine-dose-label">Dose</InputLabel>
          <Select
            labelId="medicine-dose-label"
            id="medicine-dose-select"
            value={dose}
            onChange={(e) => setDose(e.target.value)}
            label="Dose"
          >
            <MenuItem value="0.25 mcg">0.25 mcg</MenuItem>
            <MenuItem value="0.50 mcg">0.50 mcg</MenuItem>
            <MenuItem value="0.75 mcg">0.75 mcg</MenuItem>
            <MenuItem value="1.00 mcg">1.00 mcg</MenuItem>
            <MenuItem value="0.25 ml">0.25 ml</MenuItem>
            <MenuItem value="0.50 ml">0.50 ml</MenuItem>
            <MenuItem value="0.75 ml">0.75 ml</MenuItem>
            <MenuItem value="1.00 ml">1.00 ml</MenuItem>
          </Select>
        </FormControl>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-prescribed"
            label="Date Prescribed"
            value={datePrescribed}
            onChange={setDatePrescribed}
            KeyboardButtonProps={{
              "aria-label": "change date",
            }}
          />
        </MuiPickersUtilsProvider>
        <TextField
          className={classes.textField}
          id="medicine-provider"
          placeholder={medical.medical_history_provider}
          label="Prescribed by"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={provider}
          onChange={(e) => setProvider(e.target.value)}
        />
        <TextField
          className={classes.textField}
          id="medicine-description"
          placeholder={medical.medical_history_decs}
          label="Medicine Note"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          Update medical history
        </Button>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
          Medical history record updated successfully!
        </Alert>
      )}
    </div>
  );
}
