import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import { TextField, Button } from "@material-ui/core";
import Select from "@material-ui/core/Select";
import "./style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: 30,
  },
  headerColor: {
    color: "#666666",
  },
}));

export default function RadiologyUpdate(props) {
  const classes = useStyles();
  //const [healthcard, setHealthcard] = useState("");
  const [type, setType] = useState("");
  const [result, setResult] = useState("");
  const [radioimage, setImage] = useState("");
  const [date, setDate] = useState("");
  const [success, setSuccess] = useState(false);
  const [patient_radiology, setRadio] = useState({});
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/radiology";

  useEffect(() => {
    let id = props.match.params.id;
    console.log(`${URL}/record/${id}`);
    axios.get(`${URL}/record/${id}`).then(({ data }) => {
      setRadio(data.result[0]);
      console.log(data.result[0]);
    });
  }, [props.match.params.id]);

  const healthID = patient_radiology.patient_healthcard;

  const submitUpdateRadio = async (event, id) => {
    event.preventDefault();
    let updateRadio = {
      exam_type: type,
      result_desc: result,
      image: radioimage,
      date_completed: date,
    };

    try {
      axios.put(`${URL}/update/${id}`, updateRadio).then((res) => {
        console.log("Successfully updated the radiology record!");
        setRadio(updateRadio);
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/radiology/radiologyList/${healthID}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Update Radiology Test</h1>
      <form
        className="update_radiology_form"
        onSubmit={(e, b, id) => submitUpdateRadio(e, props.match.params.id)}
      >
        <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder={patient_radiology.patient_healthcard}
          label="Health Card Number"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={patient_radiology.patient_healthcard}
          
        />
        <TextField
          className={classes.textField}
          id="radiology-name"
          label="Test Type"
          variant="outlined"
          placeholder={patient_radiology.exam_type}
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={type}
          onChange={(e) => setType(e.target.value)}
        />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="radiology-result-label">Result</InputLabel>
          <Select
            labelId="radiology-result-label"
            id="radiology-result-select"
            value={result}
            onChange={(e) => setResult(e.target.value)}
            label="Status"
          >
            <MenuItem value="Positive">Positive</MenuItem>
            <MenuItem value="Negative">Negative</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={classes.textField}
          id="radiology-image"
          label="Radiology Image"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={radioimage}
          onChange={(b) => setImage(b.target.value)}
        />
        <TextField
          className={classes.textField}
          id="Test-Date"
          label="Date"
          variant="outlined"
          required
          defaultValue="2017-05-24"
          type="date"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={date}
          onChange={(e) => setDate(e.target.value)}
        />
        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          Update Radiology
        </Button>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
          Radiology updated successfully!
        </Alert>
      )}
    </div>
  );
}
