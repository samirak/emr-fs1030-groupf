import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import { TextField, Button } from "@material-ui/core";
import Select from "@material-ui/core/Select";
import "./style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: 30,
  },
  headerColor: {
    color: "#666666",
  },
}));

export default function RadiologyAdd() {
  const classes = useStyles();
  const [healthcard, setHealthcard] = useState("");
  //const [id, setID] = useState("");
  const [type, setType] = useState("");
  const [result, setResult] = useState("");
  const [radioimage, setImage] = useState("");
  const [date, setDate] = useState("");
  const [success, setSuccess] = useState(false);
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/radiology";

  const submitNewRadiology = async (event) => {
    event.preventDefault();
    let newRadiology = {      
      exam_type: type,
      result_desc: result,
      image: radioimage,
      date_completed: date,
      patient_healthcard: healthcard
    };

    try {
      axios.post(`${URL}/create`, newRadiology).then(() => {
        console.log("Successfully added a new Radiology test!");
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/radiology/radiologyList/${healthcard}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Add New Radiology Test</h1>
      <form className="register_provider_form" onSubmit={submitNewRadiology}>
        <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder="Enter Patient Health Card Number"
          label="Health Card Number"
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={healthcard}
          onChange={(e) => setHealthcard(e.target.value)}
        />
        <TextField
          className={classes.textField}
          id="exam-type"
          placeholder="Enter Exam-Type"
          label="Exam Type"
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={type}
          onChange={(e) => setType(e.target.value)}
        />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="radiology-desc">Results</InputLabel>
          <Select
            labelId="radiology-desc-label"
            id="allergy-desc-select"
            value={result}
            onChange={(e) => setResult(e.target.value)}
            label="Result"
          >
            <MenuItem value="positive">Positive</MenuItem>
            <MenuItem value="negative">Negative</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={classes.textField}
          id="radiology-image"
          label="Radiology Image"
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={radioimage}
          onChange={(b) => setImage(b.target.value)}
        />
        <TextField
          className={classes.textField}
          id="Test-Date"
          label="Date"
          variant="outlined"
          required
          defaultValue="2017-05-24"
          type="date"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={date}
          onChange={(e) => setDate(e.target.value)}
        />
        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          Add new radiology test
        </Button>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
          New radiology test added successfully!
        </Alert>
      )}
    </div>
  );
}
