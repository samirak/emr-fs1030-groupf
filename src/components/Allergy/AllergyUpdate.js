import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import {
  makeStyles,
  InputLabel,
  MenuItem,
  FormControl,
  TextField,
  Button,
  Select,
} from "@material-ui/core";
import "./style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: 30,
  },
  headerColor: {
    color: "#666666",
  },
}));

export default function AllergyUpdate(props) {
  const classes = useStyles();
  const [name, setName] = useState("");
  const [status, setStatus] = useState("");
  const [severity, setSeverity] = useState("");
  const [description, setDescription] = useState("");
  const [success, setSuccess] = useState(false);
  const [allergy, setAllergy] = useState({});
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/allergy";

  useEffect(() => {
    const id = props.match.params.id;
    
    axios.get(`${URL}/record/${id}`).then(({ data }) => {
      setAllergy(data.result[0]);
      console.log(data.result[0]);
    });
  }, [props.match.params.id]);

  const healthID = allergy.patient_healthcard;

  const submitUpdateAllergy = async (event, id) => {
    event.preventDefault();
    let updateAllergy = {
      allergy_name: name,
      allergy_status: status,
      allergy_severity: severity,
      allergy_desc: description,
    };

    try {
      axios.put(`${URL}/update/${id}`, updateAllergy).then((res) => {
        console.log("Successfully added a new allergy record!");
        setAllergy(updateAllergy);
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/allergy/list/${healthID}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Update allery</h1>
      <form
        className="update_allergy_form"
        onSubmit={(e, id) => submitUpdateAllergy(e, props.match.params.id)}
      >
      <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder={allergy.patient_healthcard}
          label="Health Card Number"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={allergy.patient_healthcard}
        />
        <TextField
          className={classes.textField}
          id="allergy-name"
          placeholder={allergy.allergy_name}
          label="Allergy Name"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="allergy-status-label">Status</InputLabel>
          <Select
            labelId="allergy-status-label"
            id="allergy-status-select"
            placeholder={allergy.allergy_status}
            value={status}
            onChange={(e) => setStatus(e.target.value)}
            label="Status"
          >
            <MenuItem value="Active">Active</MenuItem>
            <MenuItem value="Inactive">Inactive</MenuItem>
          </Select>
        </FormControl>
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="allergy-severity">Severity</InputLabel>
          <Select
            labelId="allergy-severity-label"
            id="allergy-severity-select"
            placeholder={allergy.allergy_severity}
            value={severity}
            onChange={(e) => setSeverity(e.target.value)}
            label="Severity"
          >
            <MenuItem value="Severe">Severe</MenuItem>
            <MenuItem value="Mild">Mild</MenuItem>
            <MenuItem value="Minor">Minor</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={classes.textField}
          id="allergy-description"
          placeholder={allergy.allergy_desc}
          label="Allergy Description"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          Update allergy
        </Button>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
          Allergy updated successfully!
        </Alert>
      )}
    </div>
  );
}
