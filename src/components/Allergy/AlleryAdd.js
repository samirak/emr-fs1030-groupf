import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import {
  makeStyles,
  InputLabel,
  MenuItem,
  FormControl,
  TextField,
  Button,
  Select,
} from "@material-ui/core";
import "./style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: theme.spacing(4),
  },
  headerColor: {
    color: "#666666",
  },
}));

export default function AllergyAdd() {
  const classes = useStyles();
  const [healthcard, setHealthcard] = useState("");
  const [name, setName] = useState("");
  const [status, setStatus] = useState("");
  const [severity, setSeverity] = useState("");
  const [description, setDescription] = useState("");
  const [success, setSuccess] = useState(false);
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/allergy";

  const submitNewAllergy = async (event) => {
    event.preventDefault();
    let newAllergy = {
      allergy_name: name,
      allergy_status: status,
      allergy_severity: severity,
      allergy_desc: description,
      patient_healthcard: healthcard,
    };

    try {
      axios.post(`${URL}/create`, newAllergy).then(() => {
        console.log("Successfully added a new allergy record!");
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/allergy/list/${healthcard}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Add new allery</h1>
      <form className="register_provider_form" onSubmit={submitNewAllergy}>
        <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder="Enter Patient Health Card Number"
          label="Health Card Number"
          variant="outlined"
          required
          type="text"         
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={healthcard}
          onChange={(e) => setHealthcard(e.target.value)}
        />
        <TextField
          className={classes.textField}
          id="allergy-name"
          placeholder="Enter Patient Allergy Name"
          label="Allergy Name"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="allergy-status-label">Status</InputLabel>
          <Select
            labelId="allergy-status-label"
            id="allergy-status-select"
            value={status}
            onChange={(e) => setStatus(e.target.value)}
            label="Status"
          >
            <MenuItem value="Active">Active</MenuItem>
            <MenuItem value="Inactive">Inactive</MenuItem>
          </Select>
        </FormControl>
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="allergy-severity">Severity</InputLabel>
          <Select
            labelId="allergy-severity-label"
            id="allergy-severity-select"
            value={severity}
            onChange={(e) => setSeverity(e.target.value)}
            label="Severity"
          >
            <MenuItem value="Severe">Severe</MenuItem>
            <MenuItem value="Mild">Mild</MenuItem>
            <MenuItem value="Minor">Minor</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={classes.textField}
          id="allergy-description"
          placeholder="Enter Patient Allergy Description"
          label="Allergy Description"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          Add new allergy
        </Button>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
          New allergy added successfully!
        </Alert>
      )}
    </div>
  );
}
