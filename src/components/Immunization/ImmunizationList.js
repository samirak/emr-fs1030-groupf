import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import {
  TableBody,
  TableCell,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  Button,
  TablePagination,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import "../PatientList/style.css";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";
import "../Allergy/style.css";

function ImmunizationList(props) {
  const [immunizationList, setImmunizationList] = useState("");
  const URL = "http://localhost:5500/api/patient/immunization";
  // set state for table pagination
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  // set state for modal
  const [open, setOpen] = useState(false);
  // set state for delete button
  const [btnDelete, setBtnDelete] = useState("");

  // closes modal window
  const handleClose = () => {
    setOpen(false);
  };

  // sets table pagination
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  // updates tables pagination
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  // get data from DB
  useEffect(() => {
    async function fetchData() {
      try {
        const healthID = props.match.params.healthID;

        const responseData = await axios
          .get(`${URL}/${healthID}`, {
            headers: {
              "Content-Type": "application/json",
            },
          })
          .then((response) => {
            return response;
          })
          .catch((err) => {
            console.log(err);
          });
        console.log(responseData.data.result);
        setImmunizationList(responseData.data.result);
      } catch (err) {
        console.log(err);
      }
    }
    fetchData();
  }, [props.match.params.healthID]);

  //delete one immunization by immunization id
  const deleteImmunization = (e, id) => {
    e.preventDefault();

    axios.delete(`${URL}/delete/${id}`).then((res) => {
      setImmunizationList(
        immunizationList.filter((val) => {
          return val.id !== id;
        })
      );
    });

    // set modal window to false to close
    setOpen(false);

    setTimeout(function () {
      window.location.reload();
    }, 300);
  };

  return (
    <div className="allergy-list">
      <div className="container">
        <h1>Immunization List</h1>
        <div className="addButton">
          <Link to="/api/patient/immunization/add">
            <Button variant="contained" color="primary">
              Add a new immunization
            </Button>
          </Link>
        </div>

        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Health Card Number
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Vaccine Name
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Dose
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Date Completed
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Provider
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Notes
                </TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {immunizationList &&
                immunizationList
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((p) => (
                    <TableRow key={p.immunization_id}>
                      <TableCell align="center">
                        <Link
                          to={`/api/admin/patientList/${p.patient_healthcard}`}
                        >
                          {p.patient_healthcard}
                        </Link>
                      </TableCell>
                      <TableCell align="center">
                        {p.immunization_name}
                      </TableCell>
                      <TableCell align="center">
                        {p.immunization_dose}
                      </TableCell>
                      <TableCell align="center">
                        {p.immunization_date_completed.slice(0, 10)}
                      </TableCell>
                      <TableCell align="center">
                        {p.immunization_provider}
                      </TableCell>
                      <TableCell align="center">
                        {p.immunization_desc}
                      </TableCell>
                      <TableCell align="center">
                        <Link
                          to={`/api/patient/immunization/update/${p.immunization_id}`}
                        >
                          <Button color="primary">
                            <EditIcon />
                          </Button>
                        </Link>
                        <Button
                          color="secondary"
                          onClick={(e) => {
                            setOpen(true);
                            setBtnDelete(p.immunization_id);
                          }}
                        >
                          <DeleteIcon />
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 15, 25]}
          component="div"
          count={immunizationList.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />

        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Delete Patient's Immunization Record"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <div>Are you sure you'd like to delete this immunization?</div>
              <div>You will lose this record forever!</div>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary" autoFocus>
              Disagree
            </Button>
            <Button
              onClick={(e, name) => {
                deleteImmunization(e, btnDelete);
              }}
              color="primary"
            >
              Agree
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </div>
  );
}

export default ImmunizationList;
