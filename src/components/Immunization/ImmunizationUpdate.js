import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import {
  makeStyles,
  InputLabel,
  MenuItem,
  FormControl,
  TextField,
  Button,
  Select,
} from "@material-ui/core";
import "../Allergy/style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: 30,
  },
  headerColor: {
    color: "#666666",
  },
}));

export default function ImmunizationUpdate(props) {
  const classes = useStyles();
  const [name, setName] = useState("");
  const [dose, setDose] = useState("");
  const [description, setDescription] = useState("");
  const [dateCompleted, setDateCompleted] = useState(
    new Date("2021-03-18T21:11:54")
  );
  const [provider, setProvider] = useState("");
  const [success, setSuccess] = useState(false);
  const [immunization, setImmunization] = useState({});
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/immunization";

  useEffect(() => {
    let id = props.match.params.id;
    
    axios.get(`${URL}/record/${id}`).then(({ data }) => {
      setImmunization(data.result[0]);
      console.log(data.result[0]);
    });
  }, [props.match.params.id]);

  const healthID = immunization.patient_healthcard;

  const submitUpdateImmunization = async (event, id) => {
    event.preventDefault();
    let updateImmunization = {
      immunization_name: name,
      immunization_dose: dose,
      immunization_desc: description,
      immunization_date_completed: dateCompleted,
      immunization_provider: provider,
    };

    try {
      axios.put(`${URL}/update/${id}`, updateImmunization).then((res) => {
        console.log("Successfully updated immunization record!");
        setImmunization(updateImmunization);
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/immunization/list/${healthID}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Update immunization</h1>
      <form
        onSubmit={(e, id) => submitUpdateImmunization(e, props.match.params.id)}
      >
      <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder={immunization.patient_healthcard}
          label="Health Card Number"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={immunization.patient_healthcard}
        />
        <TextField
          className={classes.textField}
          id="immunization-name"
          placeholder={immunization.immunization_name}
          label="Immunization Name"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="immunization-dose-label">Dose</InputLabel>
          <Select
            labelId="immunization-dose-label"
            id="immunization-dose-select"
            placeholder={immunization.immunization_dose}
            value={dose}
            onChange={(e) => setDose(e.target.value)}
            label="Dose"
          >
            <MenuItem value="0.25 ml">0.25 ml</MenuItem>
            <MenuItem value="0.50 ml">0.50 ml</MenuItem>
            <MenuItem value="0.75 ml">0.75 ml</MenuItem>
            <MenuItem value="1.00 ml">1.00 ml</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={classes.textField}
          id="immunization-description"
          placeholder={immunization.immunization_desc}
          label="Immunization Description"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-completed"
            label="Date Completed"
            value={dateCompleted}
            onChange={setDateCompleted}
            KeyboardButtonProps={{
              "aria-label": "change date",
            }}
          />
        </MuiPickersUtilsProvider>
        <TextField
          className={classes.textField}
          id="immunization-provider"
          placeholder={immunization.immunization_provider}
          label="Immunization Provider"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={provider}
          onChange={(e) => setProvider(e.target.value)}
        />
        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          Update immunization
        </Button>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
        Immunization record updated successfully!
        </Alert>
      )}
    </div>
  );
}
