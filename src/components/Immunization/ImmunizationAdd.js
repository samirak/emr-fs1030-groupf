import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import {
  InputLabel,
  MenuItem,
  FormControl,
  TextField,
  Button,
  Select,
  makeStyles,
} from "@material-ui/core";
import "../Allergy/style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: theme.spacing(4),
  },
}));

export default function ImmunizationAdd() {
  const classes = useStyles();
  const [healthcard, setHealthcard] = useState("");
  const [name, setName] = useState("");
  const [dose, setDose] = useState("");
  const [description, setDescription] = useState("");
  const [dateCompleted, setDateCompleted] = useState(
    new Date("2021-03-18T21:11:54")
  );
  const [provider, setProvider] = useState("");
  const [success, setSuccess] = useState(false);
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/immunization";

  const submitNewImmunization = async (event) => {
    event.preventDefault();
    let newImmunization = {
      immunization_name: name,
      immunization_dose: dose,
      immunization_desc: description,
      immunization_date_completed: dateCompleted,
      immunization_provider: provider,
      patient_healthcard: healthcard,
    };

    console.log(newImmunization);

    try {
      axios.post(`${URL}/create`, newImmunization).then(() => {
        console.log("Successfully added a new immunization record!");
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/immunization/list/${healthcard}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Add new immunization</h1>
      <form className="register_provider_form" onSubmit={submitNewImmunization}>
        <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder="Enter Patient Health Card Number"
          label="Health Card Number"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={healthcard}
          onChange={(e) => setHealthcard(e.target.value)}
        />
        <TextField
          className={classes.textField}
          id="immunization-name"
          placeholder="Enter Patient Immunization Name"
          label="Immunization Name"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="immunization-dose-label">Dose</InputLabel>
          <Select
            labelId="immunization-dose-label"
            id="immunization-dose-select"
            value={dose}
            onChange={(e) => setDose(e.target.value)}
            label="Dose"
          >
            <MenuItem value="0.25 ml">0.25 ml</MenuItem>
            <MenuItem value="0.50 ml">0.50 ml</MenuItem>
            <MenuItem value="0.75 ml">0.75 ml</MenuItem>
            <MenuItem value="1.00 ml">1.00 ml</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={classes.textField}
          id="immunization-description"
          placeholder="Enter Patient Immunization Description"
          label="Immunization Description"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-completed"
            label="Date Completed"
            value={dateCompleted}
            onChange={setDateCompleted}
            KeyboardButtonProps={{
              "aria-label": "change date",
            }}
          />
        </MuiPickersUtilsProvider>
        <TextField
          className={classes.textField}
          id="immunization-provider"
          placeholder="Enter Provider's Name"
          label="Immunization Provider"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={provider}
          onChange={(e) => setProvider(e.target.value)}
        />

        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          Add new immunization
        </Button>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
          New immunization added successfully!
        </Alert>
      )}
    </div>
  );
}
