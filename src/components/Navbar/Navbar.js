import React from "react";
import AvatarDropdown from "./AvatarDropdown";
import "../SideBar/sidebar.css";

const Navbar = () => {
  return (
    <nav className="flex justify-between px-4">
    <div className="">
          <span className="font-normal text-2xl text-gray-100">
            EMR System
          </span>
        </div>
      <div className="">        
        <AvatarDropdown />
      </div>
    </nav>
  );
};

export default Navbar;
