import React, { useState, useEffect } from "react";
import "./style.css";
import { Avatar } from "@material-ui/core";
import axios from "axios";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#4a47a3",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 16,
  },
}))(TableCell);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});
function MainPagePatientProfile(props) {
  const classes = useStyles();
  const [patientList, setPatientList] = useState([]);
  const URL = "http://localhost:5500/api/admin/patient";
  let totalUrl = window.location.pathname;
  let id = totalUrl.substring(totalUrl.lastIndexOf("/") + 1);

  useEffect(() => {
    async function fetchData() {
      try {
        let {
          data: { data },
        } = await axios.get(`${URL}`, {
          headers: {
            "Content-Type": "application/json",
          },
        });

        let patient = data.filter((item) => item.patient_healthcard === id);
        console.log(patient);
        setPatientList(patient);
      } catch (err) {
        console.log(err); 
      }
    }
    fetchData();
  }, [id]);

  // const firstName = patientList[0].patient_firstname;
  // const lastName = patientList[0].patient_lastname;

  return (
    <div className="mainPagePatientProfile-container">
      <div className="patient-title">
        <Avatar
          src="https://i2.wp.com/nofiredrills.com/wp-content/uploads/2016/10/myavatar.png?fit=400%2C400&ssl=1"
          alt="patient image"
        />

        {/* <h2>{firstName} {lastName}'s Profile</h2> */}
        <h2>Patient's Profile</h2>
      </div>
      
      <div className="personal-info">
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell align="center">Health Card</StyledTableCell>
                <StyledTableCell align="center">Name</StyledTableCell>
                <StyledTableCell align="center">Date of Birth</StyledTableCell>
                <StyledTableCell align="center">Address</StyledTableCell>
                <StyledTableCell align="center">Gender</StyledTableCell>
                <StyledTableCell align="center">Email</StyledTableCell>
                <StyledTableCell align="center">Marital</StyledTableCell>
                <StyledTableCell align="center">Insurance</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {patientList &&
                patientList.map((p) => (
                  <TableRow key={p.patient_healthcard}>
                    <StyledTableCell align="center">
                      {p.patient_healthcard}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {p.patient_firstname} {p.patient_lastname}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {p.patient_DOB}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {p.patient_street},{p.patient_city},{p.patient_province},
                      {p.patient_postalcode}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {p.patient_gender}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {p.patient_email}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {p.patient_marital}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      #{p.patient_insurance}
                    </StyledTableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
}

export default MainPagePatientProfile;
