import React from "react";
import MainPagePatientNote from "../MainPagePatientNote/MainPagePatientNote.js";
import MainPagePatientProfile from "../MainPagePatientProfile/MainPagePatientProfile";
import "./style.css";

function PatientProfile(props) {
  return (
    <div className="paitent-mainpage-container">
      <MainPagePatientProfile {...props} />
      <MainPagePatientNote {...props} />
    </div>
  );
}

export default PatientProfile;
