import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import { TextField, Button } from "@material-ui/core";
import Select from "@material-ui/core/Select";
import "./style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: 30,
  },
  headerColor: {
    color: "#666666",
  },
}));

export default function BillingUpdate(props) {
  const classes = useStyles();
  const [type, setType] = useState("");
  const [amounts, setAmounts] = useState("");
  const [referencenumber, setReferenceNumber] = useState("");
  const [bstatus, setStatus] = useState("");
  const [date, setDate] = useState("");
  const [success, setSuccess] = useState(false);
  const [patient_billing, setBilling] = useState({});
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/billing";

  useEffect(() => {
    const id = props.match.params.id;
    
    axios.get(`${URL}/record/${id}`).then(({ data }) => {
      setBilling(data.result[0]);
      console.log(data.result[0]);
    });
  }, [props.match.params.id]);

  const healthID = patient_billing.patient_healthcard;

  const submitUpdateBilling = async (event, id) => {
    event.preventDefault();
    let updateBilling = {
      transaction_type: type,
      amount: amounts,
      reference_number: referencenumber,
      status: bstatus,
      date_completed: date,
    };

    try {
      axios.put(`${URL}/update/${id}`, updateBilling).then((res) => {
        console.log("Successfully added a new billing record!");
        setBilling(updateBilling);
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/billing/billingList/${healthID}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Update Billing</h1>
      <form
        className="update_billing_form"
        onSubmit={(e, b, id) => submitUpdateBilling(e, props.match.params.id)}
      >
        <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder={patient_billing.patient_healthcard}
          label="Health Card Number"
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={patient_billing.patient_healthcard}
        />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="billing-type-label">Type</InputLabel>
          <Select
            labelId="billing-type-label"
            id="billing-type-select"
            value={type}
            onChange={(e) => setType(e.target.value)}
            label="Status"
          >
            <MenuItem value="Credit">Credit</MenuItem>
            <MenuItem value="Debit">Debit</MenuItem>
            <MenuItem value="Cash">Cash</MenuItem>
            <MenuItem value="Cheque">Cheque</MenuItem>
            <MenuItem value="eTransfer">eTransfer</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={classes.textField}
          id="billing-amount"
          label="Amount"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={amounts}
          onChange={(b) => setAmounts(b.target.value)}
        />
        <TextField
          className={classes.textField}
          id="billing-number"
          label="Reference Number"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={referencenumber}
          onChange={(b) => setReferenceNumber(b.target.value)}
        />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="billing-status-label">Status</InputLabel>
          <Select
            labelId="billing-status-label"
            id="billing-status-select"
            value={bstatus}
            onChange={(e) => setStatus(e.target.value)}
            label="Status"
          >
            <MenuItem value="Success">Success</MenuItem>
            <MenuItem value="Fail">Fail</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={classes.textField}
          id="Test-Date"
          label="Date"
          variant="outlined"
          required
          defaultValue="2017-05-24"
          type="date"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={date}
          onChange={(e) => setDate(e.target.value)}
        />
        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          Update Billing
        </Button>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
          Billing updated successfully!
        </Alert>
      )}
    </div>
  );
}
