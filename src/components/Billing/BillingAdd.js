import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import { TextField, Button } from "@material-ui/core";
import Select from "@material-ui/core/Select";
import "./style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: 30,
  },
  headerColor: {
    color: "#666666",
  },
}));

export default function BillingAdd() {
  const classes = useStyles();
  const [healthcard, setHealthcard] = useState("");
  //const [id, setID] = useState("");
  const [type, setType] = useState("");
  const [amounts, setAmounts] = useState("");
  const [referencenumber, setReferenceNumber] = useState("");
  const [bstatus, setStatus] = useState("");
  const [date, setDate] = useState("");
  const [success, setSuccess] = useState(false);
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/billing";

  const submitNewBilling = async (event) => {
    event.preventDefault();
    let newBilling = {      
    transaction_type: type,
    amount: amounts,
    reference_number: referencenumber,
    status: bstatus,
    date_completed: date,
    patient_healthcard: healthcard
    };

    try {
      axios.post(`${URL}/create`, newBilling).then(() => {
        console.log("Successfully added a new Billing!");
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/billing/billingList/${healthcard}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Add New Billing</h1>
      <form className="register_provider_form" onSubmit={submitNewBilling}>
        <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder="Enter Patient Health Card Number"
          label="Health Card Number"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={healthcard}
          onChange={(e) => setHealthcard(e.target.value)}
        />
         <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="billing-type-label">Type</InputLabel>
          <Select
            labelId="billing-type-label"
            id="billing-type-select"
            
            value={type}
            onChange={(e) => setType(e.target.value)}
            label="Status"
          >
            <MenuItem value="Credit">Credit</MenuItem>
            <MenuItem value="Debit">Debit</MenuItem>
            <MenuItem value="Cash">Cash</MenuItem>
            <MenuItem value="Cheque">Cheque</MenuItem>
            <MenuItem value="eTransfer">eTransfer</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={classes.textField}
          id="billing-amount"
          label="Amount"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={amounts}
          onChange={(b) => setAmounts(b.target.value)}
        />
        <TextField
          className={classes.textField}
          id="billing-number"
          label="Reference Number"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={referencenumber}
          onChange={(b) => setReferenceNumber(b.target.value)}
        />
       <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="billing-status-label">Status</InputLabel>
          <Select
            labelId="billing-status-label"
            id="billing-status-select"
            
            value={bstatus}
            onChange={(e) => setStatus(e.target.value)}
            label="Status"
          >
            <MenuItem value="Success">Success</MenuItem>
            <MenuItem value="Fail">Fail</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={classes.textField}
          id="Test-Date"
          label="Date"
          variant="outlined"
          required
          defaultValue="2017-05-24"
          type="date"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={date}
          onChange={(e) => setDate(e.target.value)}
        />
        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          Add Billing
        </Button>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
          New billing added successfully!
        </Alert>
      )}
    </div>
  );
}
