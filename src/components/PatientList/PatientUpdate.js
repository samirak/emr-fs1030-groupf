import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";
import { TextField, Button } from "@material-ui/core";
import "./style.css";
import { useHistory } from "react-router-dom";

const MyTextField = withStyles({
  root: {
    "& .MuiInputBase-root": {
      color: "grey",
    },
    "& .MuiFormLabel-root": {
      color: "#9C6ADE",
    },
    "& .MuiOutlinedInput-notchedOutline": {
      borderColor: "#9C6ADE",
    },
    "& .MuiFormHelperText-root": {
      color: "#9C6ADE",
    },
    "& .MuiOutlinedInput-input": {
      color: "blue",
      zIndex: "2",
    },
  },
})(TextField);

function PatientUpdate(props) {
  const [healthcard, setHealthcard] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [dateOfBirth, setDateOfBirth] = useState("");
  const [phone, setPhone] = useState("");
  const [street, setStreet] = useState("");
  const [city, setCity] = useState("");
  const [province, setProvince] = useState("");
  const [postalcode, setPosalcode] = useState("");
  const [gender, setGender] = useState("");
  const [email, setEmail] = useState("");
  const [marital, setMarital] = useState("");
  const [insurance, setInsurance] = useState(0);
  const [patient, setPatient] = useState({});
  const [update, setUpdate] = useState("Update to database");

  const history = useHistory();
  const URL = "http://localhost:5500/api/admin/patient";

  useEffect(() => {
    let id = props.match.params.id;
    console.log(`${URL}/${id}`);
    axios.get(`${URL}/${id}`).then(({ data }) => {
      setPatient(data.result[0]);
      console.log(data.result[0]);
    });
  }, [props.match.params.id]);

  const updatePatient = async (e, id) => {
    e.preventDefault();
    const updateData = {
      patient_healthcard: healthcard,
      patient_firstname: firstName,
      patient_lastname: lastName,
      patient_DOB: dateOfBirth,
      patient_phone: phone,
      patient_street: street,
      patient_city: city,
      patient_province: province,
      patient_postalcode: postalcode,
      patient_gender: gender,
      patient_email: email,
      patient_marital: marital,
      patient_insurance: insurance,
    };

    try {
      await axios.put(`${URL}/update/${id}`, updateData).then((res) => {
        console.log(res);
        setPatient(updateData);
      });

      setUpdate("Update Successfully!");
      setTimeout(function () {
        history.push("/api/admin/patientList");
      }, 3000);
    } catch (err) {
      console.log(err);
      setUpdate("Update to Database");
      throw err;
    }
  };

  return (
    <div className="container_addProvider">
      <h1>Update Patient Info </h1>
      <form
        className="register_provider_form"
        onSubmit={(e, id) => updatePatient(e, props.match.params.id)}
      >
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          placeholder={patient.patient_healthcard}
          label=" Update Health Card Number"
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={healthcard}
          onChange={(e) => setHealthcard(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          placeholder={patient.patient_firstname}
          label="Update First Name"
          required
          variant="outlined"
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update Last Name"
          placeholder={patient.patient_lastname}
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update Date of Birth"
          placeholder={patient.patient_DOB}
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={dateOfBirth}
          onChange={(e) => setDateOfBirth(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update Patient Phone (ex.123-456-7890)"
          variant="outlined"
          placeholder={patient.patient_phone}
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update Street"
          placeholder={patient.patient_street}
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={street}
          onChange={(e) => setStreet(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update City"
          placeholder={patient.patient_city}
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={city}
          onChange={(e) => setCity(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update Province"
          placeholder={patient.patient_province}
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={province}
          onChange={(e) => setProvince(e.target.value)}
        />

        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update postalcode"
          placeholder={patient.patient_postalcode}
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={postalcode}
          onChange={(e) => setPosalcode(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update Gender"
          placeholder={patient.patient_gender}
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={gender}
          onChange={(e) => setGender(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update Email Address"
          placeholder={patient.patient_email}
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update Married Status"
          placeholder={patient.patient_marital}
          variant="outlined"
          type="text"
          required
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={marital}
          onChange={(e) => setMarital(e.target.value)}
        />
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update Insurance Number (ex.123456884)"
          variant="outlined"
          type="number"
          required
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={insurance}
          onChange={(e) => setInsurance(e.target.value)}
        />
        <div className="">
          <Button
            type="submit"
            variant="outlined"
            color="primary"
            boxShadow={3}
          >
            {update}
          </Button>
        </div>
      </form>
    </div>
  );
}

export default PatientUpdate;
