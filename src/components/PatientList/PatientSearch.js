import React, { useState, useEffect } from "react";
import axios from "axios";
import { StyledContainer, StyledFormArea } from "./style.js";
import {
  TableBody,
  TableCell,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TextField,
  Button,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import "./style.css";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";
import "./style.js";
import { withStyles } from "@material-ui/core/styles";

const MyTextField = withStyles({
  root: {
    "& .MuiInputBase-root": {
      color: "grey",
    },    
    "& .MuiOutlinedInput-notchedOutline": {
      borderColor: "#9C6ADE",
    },    
  },
})(TextField);

function PatientSearch() {
  const [patient, setPatient] = useState([]);
  const [healthCard, setHealthCard] = useState("");
  const URL = "http://localhost:5500/api/admin/patient";

  // get data from DB
  useEffect(() => {
    async function fetchData() {
      try {
        let {
          data: { data },
        } = await axios.get(`${URL}`, {
          headers: {
            "Content-Type": "application/json",
          },
        });
        console.log("a", Array.from(data));
        console.log(healthCard);
        let search = Array.from(data).filter(
          (item) => item.patient_healthcard === healthCard
        );
        setPatient(search);
        console.log(patient);
      } catch (err) {
        console.log(err);
      }
    }
    fetchData();
  }, [healthCard]);
  //delete one patient by healthcard id
  const deletePatient = (e, id) => {
    e.preventDefault();
    axios.delete(`${URL}/delete/${id}`).then((res) => {
      setPatient(
        patient.filter((val) => {
          return val.id !== id;
        })
      );
      alert(`Delete id =${id} care provider successfully`);
    });
    setTimeout(function () {
      window.location.reload();
    }, 300);
  };
  return (
    <div className="patient-list">
      <StyledContainer style={{ position: "relative" }}>
        <StyledFormArea
          style={{ backgroundColor: "#fff", borderRadius: "10px" }}
        >
          <div>
            <h1>Search a Patient's Info</h1>
            <form
              className="register_provider_form"
              // onSubmit={(e, id) => findPatient(e, props.match.params.id)}
            >
              <MyTextField
                style={{ marginBottom: "30px" }}
                id="standard-full-width"
                placeholder="111-111-1111"
                label="Input Health Card 10 Number"
                variant="outlined"
                type="text"
                required
                fullWidth
                InputLabelProps={{
                  shrink: true,
                }}
                value={healthCard}
                onChange={(e) => setHealthCard(e.target.value)}
                color="primary"
              />
            </form>
            {patient.length > 0 && (
              <TableContainer component={Paper}>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell
                        align="center"
                        style={{ fontWeight: "700", color: "#230051" }}
                      >
                        Health Card Number
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ fontWeight: "700", color: "#230051" }}
                      >
                        Patient Name
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ fontWeight: "700", color: "#230051" }}
                      >
                        Date Of Birth <br></br>(yyyy-mm-dd)
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ fontWeight: "700", color: "#230051" }}
                      >
                        Patient Phone
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ fontWeight: "700", color: "#230051" }}
                      >
                        Patient Address
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ fontWeight: "700", color: "#230051" }}
                      >
                        Gender
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ fontWeight: "700", color: "#230051" }}
                      >
                        Patient Email
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ fontWeight: "700", color: "#230051" }}
                      >
                        Marital
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ fontWeight: "700", color: "#230051" }}
                      >
                        Insurance Number
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{ fontWeight: "700", color: "#230051" }}
                      >
                        Edit / Delete
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {patient &&
                      patient.map((p) => (
                        <TableRow>
                          <TableCell align="center">
                            <Link className="healthCardLink"
                              to={`/api/admin/patientList/${p.patient_healthcard}`}
                            >
                              {p.patient_healthcard}
                            </Link>
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_firstname} {p.patient_lastname}
                          </TableCell>
                          <TableCell align="center">{p.patient_DOB}</TableCell>
                          <TableCell align="center">
                            {p.patient_phone}
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_street},{p.patient_city},
                            {p.patient_province},{p.patient_postalcode}
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_gender}
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_email}
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_marital}
                          </TableCell>
                          <TableCell align="center">
                            {p.patient_insurance}
                          </TableCell>
                          <Link
                            to={`/api/admin/patient/update/${p.patient_healthcard}`}
                          >
                            <Button color="primary">
                              <EditIcon />
                            </Button>
                          </Link>

                          <Button
                            color="secondary"
                            onClick={(e, id) => {
                              deletePatient(e, p.patient_healthcard);
                            }}
                          >
                            <DeleteIcon />
                          </Button>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </TableContainer>
            )}
          </div>
        </StyledFormArea>
      </StyledContainer>
    </div>
  );
}

export default PatientSearch;
