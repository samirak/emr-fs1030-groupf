import React, { useState, useEffect } from "react";
import {
  TableBody,
  TableCell,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  Button,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import axios from "axios";
import "./style.css";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";

function PatientList() {
  const [patientList, setPatientList] = useState([]);
  const URL = "http://localhost:5500/api/admin/patient";

  // get data from DB
  useEffect(() => {
    async function fetchData() {
      try {
        let {
          data: { data },
        } = await axios.get(`${URL}`, {
          headers: {
            "Content-Type": "application/json",
          },
        });
        setPatientList(Array.from(data));
        console.log(data);
      } catch (err) {
        console.log(err);
      }
    }
    fetchData();
  }, []);

  //delete one patient by healthcard id
  const deletePatient = (e, id) => {
    e.preventDefault();
    axios.delete(`${URL}/delete/${id}`).then((res) => {
      setPatientList(
        patientList.filter((val) => {
          return val.id !== id;
        })
      );
      alert(`Delete id =${id} care provider successfully`);
    });
    setTimeout(function () {
      window.location.reload();
    }, 300);
  };

  return (
    <div className="">
      <div className="patient-list">
        <h1>Patients List</h1>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Health Card Number
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Patient Name
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Date Of Birth <br></br>(yyyy-mm-dd)
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Patient Phone
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Patient Address
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Gender
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Patient Email
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Marital
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Insurance Number
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Edit / Delete
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {patientList &&
                patientList.map((p) => (
                  <TableRow>
                    <TableCell align="center">
                      <Link
                        to={`/api/admin/patientList/${p.patient_healthcard}`}
                      >
                        {p.patient_healthcard}
                      </Link>
                    </TableCell>
                    <TableCell align="center">
                      {p.patient_firstname} {p.patient_lastname}
                    </TableCell>
                    <TableCell align="center">{p.patient_DOB}</TableCell>
                    <TableCell align="center">{p.patient_phone}</TableCell>
                    <TableCell align="center">
                      {p.patient_street},{p.patient_city},{p.patient_province},
                      {p.patient_postalcode}
                    </TableCell>
                    <TableCell align="center">{p.patient_gender}</TableCell>
                    <TableCell align="center">{p.patient_email}</TableCell>
                    <TableCell align="center">{p.patient_marital}</TableCell>
                    <TableCell align="center">{p.patient_insurance}</TableCell>
                    <TableCell align="center">
                      <Link
                        to={`/api/admin/patient/update/${p.patient_healthcard}`}
                      >
                        <Button color="primary">
                          <EditIcon />
                        </Button>
                      </Link>

                      <Button
                        color="secondary"
                        onClick={(e, id) => {
                          deletePatient(e, p.patient_healthcard);
                        }}
                      >
                        <DeleteIcon />
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
}

export default PatientList;
