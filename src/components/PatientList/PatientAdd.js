import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";
import { TextField, Button } from "@material-ui/core";
import "./style.css";
import { useHistory } from "react-router-dom";
const MyTextField = withStyles({
  root: {
    "& .MuiInputBase-root": {
      color: "grey",
    },    
    "& .MuiFormHelperText-root": {
      color: "#9C6ADE",
    },
  },
})(TextField);


function PatientAdd() {
  const [healthcard, setHealthcard] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [dateOfBirth, setDateOfBirth] = useState("");
  const [phone, setPhone] = useState("");
  const [street, setStreet] = useState("");
  const [city, setCity] = useState("");
  const [province, setProvince] = useState("");
  const [postalcode, setPosalcode] = useState("");
  const [gender, setGender] = useState("");
  const [email, setEmail] = useState("");
  const [marital, setMarital] = useState("");
  const [insurance, setInsurance] = useState(0);
  const [add, setAdd] = useState("Add to database");
  const history = useHistory();
  const URL = "http://localhost:5500/api/admin/patient";

  const submitNewPatient = async (e) => {
    e.preventDefault();
    let newPatient = {
      patient_healthcard: healthcard,
      patient_firstname: firstName,
      patient_lastname: lastName,
      patient_DOB: dateOfBirth,
      patient_phone: phone,
      patient_street: street,
      patient_city: city,
      patient_province: province,
      patient_postalcode: postalcode,
      patient_gender: gender,
      patient_email: email,
      patient_marital: marital,
      patient_insurance: insurance,
    };

    try {
      await axios.post(`${URL}/create`, newPatient).then(() => {
        console.log("successful Add a new patient");
      });

      setAdd("Add Successfully!");
      setTimeout(function () {
        history.push("/api/admin/patientList");
      }, 3000);
    } catch (err) {
      console.log(err);
      setAdd("Add to Database");
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Register a new patient</h1>
      <form className="register_provider_form" onSubmit={submitNewPatient}>
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          placeholder="Enter Patient Health Card Number"
          label="Health Card Number"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={healthcard}
          onChange={(e) => setHealthcard(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          placeholder="Enter First Name"
          label="First Name"
          required
          variant="outlined"
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Last Name"
          placeholder="Enter Last Name"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Date of Birth"
          placeholder="yyyy-mm-dd"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={dateOfBirth}
          onChange={(e) => setDateOfBirth(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Patient Phone (ex.123-456-7890)"
          placeholder="000-000-0000"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Patient Street"
          placeholder="Enter Street"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={street}
          onChange={(e) => setStreet(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Patient City"
          placeholder="Enter City"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={city}
          onChange={(e) => setCity(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Patient Province"
          placeholder="Enter Province"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={province}
          onChange={(e) => setProvince(e.target.value)}
        />

        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Patient postalcode"
          placeholder="R3P 4S8"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={postalcode}
          onChange={(e) => setPosalcode(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Gender"
          placeholder="Female / Male"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={gender}
          onChange={(e) => setGender(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Email Address"
          placeholder="sam@gmail.com"
          variant="outlined"
          required
          type="text"          
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Married Status"
          placeholder="single / married "
          variant="outlined"
          type="text"
          required
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={marital}
          onChange={(e) => setMarital(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Insurance Number (ex.123456884)"
          variant="outlined"
          type="number"
          required
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={insurance}
          onChange={(e) => setInsurance(e.target.value)}
        />

        <div>
          <Button
            type="submit"
            variant="outlined"
            color="primary"
            boxShadow={3}
          >
            {add}
          </Button>
        </div>
      </form>
    </div>
  );
}

export default PatientAdd;
