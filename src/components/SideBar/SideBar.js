import React, { useContext } from 'react';
import classNames from 'classnames';
import HealingOutlinedIcon from '@material-ui/icons/HealingOutlined';
import { Link, useLocation } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';
import "./sidebar.css";

const NavItem = ({ navItem }) => {
  const location = useLocation();
  const isCurrentRoute =
    location.pathname === `/${navItem.path}`;
  const classes = classNames({
    'px-2 sm:px-6 justify-center sm:justify-start py-3 flex uppercase': true,
    'text-indigo-700 hover:text-blue-500 transform hover:translate-x-1 transition ease-in-out duration-100': !isCurrentRoute,
    'text-gray-600': isCurrentRoute
  });
  return (
    <Link to={navItem.path} className={classes}>
      <div className="flex items-center">
        <div className="mr-0 sm:mr-4">
          <HealingOutlinedIcon />
        </div>
        <span className="hidden sm:block">
          {navItem.label}
        </span>
        <hr />
      </div>
    </Link>
  );
};

const NavItemContainer = ({ children }) => (
  <div>{children}</div>
);

const SideBar = () => {
  const auth = useContext(AuthContext);
  const { user_role } = auth.authState.userInfo;
  let totalUrl = window.location.pathname;
  let id = totalUrl.substring(totalUrl.lastIndexOf("/") + 1);
  const navItems = [
    {
      label: 'Search for a patient',
      path: '/patient/search',    
      allowedRoles: ['User']
    },
    {
      label: 'Patient Profile',
      path: `/api/patient/profile/${id}`,
      allowedRoles: ['User']
    },
    {
      label: 'Patient Medical History',
      path: `/api/patient/medical/list/${id}`,    
      allowedRoles: ['User']
    },
    {
      label: 'Patient Allergies',
      path: `/api/patient/allergy/list/${id}`,    
      allowedRoles: ['User']
    },
    {
      label: 'Patient Immunization',
      path: `/api/patient/immunization/list/${id}`,    
      allowedRoles: ['User']
    },
    {
      label: 'Patient Lab Test',
      path: `/api/patient/testing/testingList/${id}`,    
      allowedRoles: ['User']
    },
    {
      label: 'Patient Radiology',
      path: `/api/patient/radiology/radiologyList/${id}`,    
      allowedRoles: ['User']
    },
    {
      label: 'Patient billing',
      path: `/api/patient/billing/billingList/${id}`,    
      allowedRoles: ['User']
    },
    {
      label: 'Search for a patient',
      path: '/api/admin/patient/search',    
      allowedRoles: ['Admin']
    },
    {
      label: 'List of all patients',
      path: '/api/admin/patientList',    
      allowedRoles: ['Admin']
    },
    {
      label: 'Register a new patient',
      path: '/api/admin/patientAdd',    
      allowedRoles: ['Admin']
    },
    {
      label: 'Search for a care provider',
      path: '/admin/api/provider/search',    
      allowedRoles: ['Admin']
    },
    {
      label: 'List of all care providers',
      path: '/admin/api/provider',    
      allowedRoles: ['Admin']
    },
    {
      label: 'Register a new care provider',
      path: '/admin/api/providerAdd',    
      allowedRoles: ['Admin']
    }
  ];
  
  return (
    <section className="h-auto">      
      <div className="mt-16">
        {navItems.map((navItem, i) => (
          <NavItemContainer key={i}>
            {navItem.allowedRoles.includes(user_role) && (
              <NavItem navItem={navItem} />
            )}
          </NavItemContainer>
        ))}
      </div>
    </section>
  );
};

export default SideBar;
