import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { TextField, Button, makeStyles } from "@material-ui/core";
import "../Allergy/style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: 30,
  },
}));

export default function PatientNoteUpdate(props) {
  const classes = useStyles();
  const [noteDescription, setNoteDescription] = useState("");
  const [noteDate, setNoteDate] = useState(new Date("2021-03-18T21:11:54"));

  const [success, setSuccess] = useState(false);
  const [note, setNote] = useState({});
  const [healthcard, setHealthcard] = useState("");
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/note";

  useEffect(() => {
    let id = props.match.params.id;
    console.log(`${URL}/${id}`);
    axios.get(`${URL}/${id}`).then(({ data }) => {
      setNote(data.result[0]);
      console.log(data.result[0]);
    });
  }, [props.match.params.id]);

  const submitUpdateNote = async (event, id) => {
    event.preventDefault();
    let updateNote = {
      patient_note: noteDescription,
      date_created: noteDate,
      patient_healthcard: healthcard,
    };

    try {
      axios.put(`${URL}/update/${id}`, updateNote).then((res) => {
        console.log("Successfully updated Patient Note!");
        setNote(updateNote);
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/profile/${healthcard}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Update Patient Note</h1>
      <form onSubmit={(e, id) => submitUpdateNote(e, props.match.params.id)}>
        <TextField
          className={classes.textField}
          id="patient-health-card"
          label="Health Card Number"
          variant="outlined"          
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={healthcard}
          onChange={(e) => setHealthcard(e.target.value)}
        />
        <TextField
          className={classes.textField}
          id="note-description"
          placeholder="Enter Note Description"
          label="Patient Note"
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={noteDescription}
          onChange={(e) => setNoteDescription(e.target.value)}
        />

        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-prescribed"
            label="Date Prescribed"
            value={noteDate}
            onChange={setNoteDate}
            KeyboardButtonProps={{
              "aria-label": "change date",
            }}
          />
        </MuiPickersUtilsProvider>
        <div>
          <Button
            type="submit"
            variant="outlined"
            color="primary"
            boxShadow={3}
          >
            Update Patient Note
          </Button>
        </div>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
          Patient Note updated successfully!
        </Alert>
      )}
    </div>
  );
}
