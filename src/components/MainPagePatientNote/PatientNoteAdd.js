import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { TextField, Button, makeStyles } from "@material-ui/core";
import "../Allergy/style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: theme.spacing(4),
  },
}));

export default function PatientNoteAdd() {
  const classes = useStyles();
  const [healthcard, setHealthcard] = useState("");
  const [noteDescription, setNoteDescription] = useState("");
  const [noteDate, setNoteDate] = useState(new Date("2021-03-18T21:11:54"));
  const [success, setSuccess] = useState(false);
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/Note";

  const submitNewNote = async (event) => {
    event.preventDefault();
    let newPatientNote = {
      patient_healthcard: healthcard,
      patient_note: noteDescription,
      date_created: noteDate,
    };

    console.log(newPatientNote);

    try {
      axios.post(`${URL}/create`, newPatientNote).then(() => {
        console.log("Successfully added a new Patient Note!");
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/profile/${healthcard}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Add new Patient Note</h1>
      <form onSubmit={submitNewNote}>
        <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder="Enter Patient Health Card Number"
          label="Health Card Number"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={healthcard}
          onChange={(e) => setHealthcard(e.target.value)}
        />
        <TextField
          className={classes.textField}
          id="note-description"
          placeholder="Enter Note Description"
          label="Patient Note"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={noteDescription}
          onChange={(e) => setNoteDescription(e.target.value)}
        />

        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="MM/dd/yyyy"
            margin="normal"
            id="date-prescribed"
            label="Date Prescribed"
            value={noteDate}
            onChange={setNoteDate}
            KeyboardButtonProps={{
              "aria-label": "change date",
            }}
          />
        </MuiPickersUtilsProvider>
        <div>
          <Button
            type="submit"
            variant="outlined"
            color="primary"
            boxShadow={3}
          >
            Add new Patient Note
          </Button>
        </div>
      </form>
      {success && (
        <Alert variant="outlined" severity="success" className="alert-success">
          New Patient Note added successfully!
        </Alert>
      )}
    </div>
  );
}
