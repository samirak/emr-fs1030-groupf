import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import { TextField, Button } from "@material-ui/core";
import Select from "@material-ui/core/Select";
import "./style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: 30,
  },
  headerColor: {
    color: "#666666",
  },
}));

export default function TestingUpdate(props) {
  const classes = useStyles();
  const [testname, setName] = useState("");
  const [result, setResult] = useState("");
  const [posneg, setPosneg] = useState("");
  const [date, setDate] = useState("");
  const [success, setSuccess] = useState(false);
  const [lab_test, setTesting] = useState({});
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/testing";

  useEffect(() => {
    let id = props.match.params.id;
    
    axios.get(`${URL}/record/${id}`).then(({ data }) => {
      setTesting(data.result[0]);
      console.log(data.result[0]);
    });
  }, [props.match.params.id]);

  const healthID = lab_test.patient_healthcard;

  const submitUpdateTesting = async (event, id) => {
    event.preventDefault();
    let updateTesting = {
      test_name: testname,
      result_desc: result,
      positive_negative: posneg,
      date_completed: date,
    };

    try {
      axios.put(`${URL}/update/${id}`, updateTesting).then((res) => {
        console.log("Successfully added a new test!");
        setTesting(updateTesting);
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/testing/testingList/${healthID}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Update Test</h1>
      <form
        className="update_testing_form"
        onSubmit={(e, b, id) => submitUpdateTesting(e, props.match.params.id)}
      >
        <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder={lab_test.patient_healthcard}
          label="Health Card Number"
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={lab_test.patient_healthcard}
        />
        <TextField
          className={classes.textField}
          id="testing-name"
          label="Test Name"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={testname}
          onChange={(e) => setName(e.target.value)}
        />
        <TextField
          className={classes.textField}
          id="testing-description"
          label="Results"
          variant="outlined"
          required
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={result}
          onChange={(b) => setResult(b.target.value)}
        />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="testing-result-label">Pos/Neg</InputLabel>
          <Select
            labelId="testing-result-label"
            id="testing-result-select"
            value={posneg}
            onChange={(e) => setPosneg(e.target.value)}
            label="Status"
          >
            <MenuItem value="Positive">Positive</MenuItem>
            <MenuItem value="Negative">Negative</MenuItem>
          </Select>
        </FormControl>

        <TextField
          className={classes.textField}
          id="Test-Date"
          label="Date"
          variant="outlined"
          required
          defaultValue="2017-05-24"
          type="date"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={date}
          onChange={(e) => setDate(e.target.value)}
        />
        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          Update Test
        </Button>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
          Test updated successfully!
        </Alert>
      )}
    </div>
  );
}
