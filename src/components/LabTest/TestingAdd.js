import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import { TextField, Button } from "@material-ui/core";
import Select from "@material-ui/core/Select";
import "./style.css";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles((theme) => ({
  formControl: {
    marginBottom: theme.spacing(4),
    minWidth: 120,
  },
  textField: {
    marginBottom: 30,
  },
  headerColor: {
    color: "#666666",
  },
}));

export default function TestingAdd() {
  const classes = useStyles();
  const [healthcard, setHealthcard] = useState("");
  //const [id, setID] = useState("");
  const [testname, setTestname] = useState("");
  const [result, setResult] = useState("");
  const [posneg, setPosneg] = useState("");
  const [date, setDate] = useState("");
  const [success, setSuccess] = useState(false);
  const history = useHistory();
  const URL = "http://localhost:5500/api/patient/testing";

  const submitNewTesting = async (event) => {
    event.preventDefault();
    let newTesting = {
      test_name: testname,
      result_desc: result,
      positive_negative: posneg,
      date_completed: date,
      patient_healthcard: healthcard,
    };

    try {
      axios.post(`${URL}/create`, newTesting).then(() => {
        console.log("Successfully added a new test!");
        setSuccess(true);
      });

      setTimeout(function () {
        history.push(`/api/patient/testing/testingList/${healthcard}`);
      }, 3000);
    } catch (err) {
      console.log(err);
      setSuccess(false);
      throw err;
    }
  };

  return (
    <div className="container_patient">
      <h1>Add New Test</h1>
      <form className="register_provider_form" onSubmit={submitNewTesting}>
        <TextField
          className={classes.textField}
          id="patient-health-card"
          placeholder="Enter Patient Health Card Number"
          label="Health Card Number"
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={healthcard}
          onChange={(e) => setHealthcard(e.target.value)}
        />
        <TextField
          className={classes.textField}
          id="testing-name"
          label="Test Name"
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={testname}
          onChange={(e) => setTestname(e.target.value)}
        />
        <TextField
          className={classes.textField}
          id="testing-description"
          label="Results"
          variant="outlined"
          required
          type="text"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={result}
          onChange={(b) => setResult(b.target.value)}
        />
        <FormControl
          variant="outlined"
          className={classes.formControl}
          fullWidth
        >
          <InputLabel id="testing-result-label">Pos/Neg</InputLabel>
          <Select
            labelId="testing-result-label"
            id="testing-result-select"
            value={posneg}
            onChange={(e) => setPosneg(e.target.value)}
            label="Status"
          >
            <MenuItem value="Positive">Positive</MenuItem>
            <MenuItem value="Negative">Negative</MenuItem>
          </Select>
        </FormControl>

        <TextField
          className={classes.textField}
          id="Test-Date"
          label="Date"
          variant="outlined"
          required
          defaultValue="2017-05-24"
          type="date"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={date}
          onChange={(e) => setDate(e.target.value)}
        />
        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          Add Test
        </Button>
      </form>
      {success && (
        <Alert variant="outlined" severity="success">
          New radiology test added successfully!
        </Alert>
      )}
    </div>
  );
}
