import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import {
  TableBody,
  TableCell,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  Button,
  TablePagination,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import "../PatientList/style.css";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Paper from "@material-ui/core/Paper";
import "./style.css";

function TestingList(props) {
  const [testingList, setTestingList] = useState("");
  const URL = "http://localhost:5500/api/patient/testing";
  // set state for table pagination
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  // set state for modal
  const [open, setOpen] = useState(false);
  // set state for delete button
  const [btnDelete, setBtnDelete] = useState("");

  // closes modal window
  const handleClose = () => {
    setOpen(false);
  };

  // sets table pagination
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  // updates tables pagination
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  // get data from DB
  useEffect(() => {
    async function fetchData() {
      try {
        const healthID = props.match.params.healthID;

        const responseData = await axios
          .get(`${URL}/${healthID}`, {
            headers: {
              "Content-Type": "application/json",
            },
          })
          .then((response) => {
            return response;
          })
          .catch((err) => {
            console.log(err);
          });
        console.log(responseData.data.result);
        setTestingList(responseData.data.result);
      } catch (err) {
        console.log(err);
      }
    }
    fetchData();
  }, [props.match.params.healthID]);

  //delete one allergy by healthcard id
  const deleteTesting = (e, id) => {
    e.preventDefault();

    axios.delete(`${URL}/delete/${id}`).then((res) => {
      setTestingList(
        TestingList.filter((val) => {
          return val.id !== id;
        })
      );
    });

    // set modal window to false to close
    setOpen(false);

    setTimeout(function () {
      window.location.reload();
    }, 300);
  };

  return (
    <div className="testing-list">
      <div className="container">
        <h1>Lab Test</h1>
        <div className="addButton">
          <Link to="/api/patient/testing/testingAdd">
            <Button variant="contained" color="primary">
              Add a new lab test
            </Button>
          </Link>
        </div>

        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Health Card Number
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Test
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Results
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Positive or Negative
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Date Completed
                </TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {testingList &&
                testingList
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((p) => (
                    <TableRow key={p.lab_id}>
                      <TableCell align="center">
                        <Link
                          to={`/api/admin/patientList/${p.patient_healthcard}`}
                        >
                          {p.patient_healthcard}
                        </Link>
                      </TableCell>
                      <TableCell align="center">{p.test_name}</TableCell>
                      <TableCell align="center">{p.result_desc}</TableCell>
                      <TableCell align="center">
                        {p.positive_negative}
                      </TableCell>
                      <TableCell align="center">{p.date_completed}</TableCell>
                      <TableCell align="center">
                        <Link to={`/api/patient/testing/update/${p.lab_id}`}>
                          <Button color="primary">
                            <EditIcon />
                          </Button>
                        </Link>
                        <Button
                          color="secondary"
                          onClick={(e) => {
                            setOpen(true);
                            setBtnDelete(p.lab_id);
                          }}
                        >
                          <DeleteIcon />
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 15, 25]}
          component="div"
          count={TestingList.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />

        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Delete Patient's Test"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <div>Are you sure you'd like to delete this test?</div>
              <div>You will lose this record forever!</div>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary" autoFocus>
              Disagree
            </Button>
            <Button
              onClick={(e, lab_id) => {
                deleteTesting(e, btnDelete);
              }}
              color="primary"
            >
              Agree
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </div>
  );
}

export default TestingList;
