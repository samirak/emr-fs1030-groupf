import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
}));

export default function SignUp() {
  const classes = useStyles();

  return (
    <>
      <Container component="main" maxWidth="s">
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h1">
            404
          </Typography>
          <Typography component="h2" variant="h2">
            Page Not Found
          </Typography>
          <Grid container justify="center">
            <Grid item>
              <Link href="/" variant="body" className={classes.paper}>
                Go Back Home
              </Link>
            </Grid>
          </Grid>
        </div>
      </Container>
    </>
  );
}
