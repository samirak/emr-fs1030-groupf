import React, { useState, useEffect } from "react";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";
import { TextField, Button } from "@material-ui/core";
import "./style.css";
import { useHistory } from "react-router-dom";

const MyTextField = withStyles({
  root: {
    "& .MuiInputBase-root": {
      color: "grey",
    },
    "& .MuiFormLabel-root": {
      color: "#9C6ADE",
    },
    "& .MuiOutlinedInput-notchedOutline": {
      borderColor: "#9C6ADE",
    },
    "& .MuiFormHelperText-root": {
      color: "#9C6ADE",
    },
    "& .MuiOutlinedInput-input": {
      color: "blue",
      zIndex: "2",
    },
  },
})(TextField);

const UpdateBtn = withStyles({
  root: {
    "& .MuiButton-label": {
      color: "#000000",
      fontWeight: "600",
    },
    "&:hover .MuiButton-label": {
      color: "#ED6347",
      fontWeight: "600",
    },
  },
})(Button);

function ProviderUpdate(props) {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [userName, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isAdmin, setIsAdmin] = useState(0);
  const [avatar, setAvatar] = useState("");
  const [provider, setProvider] = useState("");
  const [update, setUpdate] = useState("Update to database");
  const history = useHistory();
  const URL = "http://localhost:5500/api/provider";

  useEffect(() => {
    let id = props.match.params.id;
    axios.get(`${URL}/${id}`).then(({ data }) => {
      setProvider(data[0]);
      console.log(data[0]);
    });
  }, [props.match.params.id]);

  const updateProvider = (e, id) => {
    e.preventDefault();
    let newData = {
      careprovider_firstname: firstName,
      careprovider_lastname: lastName,
      careprovider_username: userName,
      careprovider_password: password,
      careprovider_avatar: avatar,
      is_admin: isAdmin,
    };

    try {
      axios.put(`${URL}/update/${id}`, newData).then((res) => {
        setProvider(newData);
        console.log(res);
      });
      setUpdate("Update Successfully!");
      setTimeout(function () {
        history.push("/admin/api/provider");
      }, 3000);
    } catch (err) {
      console.log(err);
      setUpdate("Update to Database");
      throw err;
    }
  };

  return (
    <div className="container_addProvider">
      <h1>Update Care Provider's Info </h1>
      <form
        className="register_provider_form"
        onSubmit={(e, id) => updateProvider(e, props.match.params.id)}
      >
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          placeholder={provider.careprovider_firstname}
          label="Update careprovider first name"
          variant="outlined"
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          color="primary"
        />

        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update careprovider last name"
          placeholder={provider.careprovider_lastname}
          variant="outlined"
          type="text"
          helperText="This field is required"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update careprovider username"
          placeholder={provider.careprovider_username}
          variant="outlined"
          type="text"
          helperText="This field is required"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={userName}
          onChange={(e) => setUsername(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update careprovider password"
          placeholder="Input new password"
          variant="outlined"
          type="number"
          helperText="This field is required"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />

        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update careprovider avatar"
          placeholder={provider.careprovider_avatar}
          variant="outlined"
          type="text"
          helperText="This field is required"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={avatar}
          onChange={(e) => setAvatar(e.target.value)}
        />

        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Update careprovider is an admin? (1 = admin)"
          placeholder={provider.is_admin}
          variant="outlined"
          type="text"
          helperText="This field is required"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={isAdmin}
          onChange={(e) => setIsAdmin(e.target.value)}
        />

        <div className="button--container">
          <UpdateBtn
            type="submit"
            variant="outlined"
            color="primary"
            boxShadow={3}
          >
            {update}
          </UpdateBtn>
        </div>
      </form>
    </div>
  );
}

export default ProviderUpdate;
