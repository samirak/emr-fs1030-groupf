import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import axios from "axios";
import { TextField, Button } from "@material-ui/core";
import "./style.css";
import { useHistory } from "react-router-dom";
const MyTextField = withStyles({
  root: {
    "& .MuiInputBase-root": {
      color: "grey",
    },    
  },
})(TextField);


function ProviderAdd() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [userName, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isAdmin, setIsAdmin] = useState(0);
  const [avatar, setAvatar] = useState("");
  const [add, setAdd] = useState("Add to database");
  const history = useHistory();
  const URL = "http://localhost:5500/api/provider";

  const submitNewProvider = async (e) => {
    e.preventDefault();
    let provider = {
      careprovider_firstname: firstName,
      careprovider_lastname: lastName,
      careprovider_username: userName,
      careprovider_password: password,
      careprovider_avatar: avatar,
      is_admin: isAdmin,
    };

    try {
      await axios.post(`${URL}/create`, provider).then(() => {
        console.log("successful Add a new Care Provider");
      });

      setAdd("Add Successfully!");
      setTimeout(function () {
        history.push("/admin/api/provider");
      }, 3000);
    } catch (err) {
      console.log(err);
      setAdd("Add to Database");
      throw err;
    }
  };

  return (
    <div className="container_addProvider">
      <h1>Register a Care Provider</h1>
      <form className="register_provider_form" onSubmit={submitNewProvider}>
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          placeholder="Enter First Name"
          label="First Name"
          variant="outlined"
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        />

        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Last Name"
          placeholder="Enter Last Name"
          variant="outlined"
          required
          type="text"
          helperText="This field is required"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="User Name"
          placeholder="Enter User Name"
          variant="outlined"
          required
          type="text"
          helperText="This field is required"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={userName}
          onChange={(e) => setUsername(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Password"
          placeholder="Enter Password"
          variant="outlined"
          required
          type="number"
          helperText="This field is required"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          placeholder="Enter Avatar Link"
          label="Avatar"
          variant="outlined"
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={avatar}
          onChange={(e) => setAvatar(e.target.value)}
        />
        <MyTextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="isAdmin ? 1=Admin, 0=care provider"
          placeholder="isAdmin"
          variant="outlined"
          required
          type="number"
          helperText="This field is required"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={isAdmin}
          onChange={(e) => setIsAdmin(e.target.value)}
        />

        <div className="button--container">
          <Button
            type="submit"
            variant="outlined"
            color="primary"
            boxShadow={3}
          >
            {add}
          </Button>
        </div>
      </form>
    </div>
  );
}

export default ProviderAdd;
