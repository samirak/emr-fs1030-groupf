import React, { useState, useEffect } from "react";
import { StyledContainer, StyledFormArea } from "./style.js";
import {
  TableBody,
  TableCell,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  Button,
} from "@material-ui/core";
import axios from "axios";
import { Link } from "react-router-dom";
import Paper from "@material-ui/core/Paper";

function ProviderList() {
  const [providerList, setProviderList] = useState([]);
  const URL = "http://localhost:5500/api/provider";
  useEffect(() => {
    async function fetchData() {
      try {
        let {
          data: { data },
        } = await axios.get(`${URL}`, {
          headers: {
            "Content-Type": "application/json",
          },
        });
        setProviderList(Array.from(data));
        console.log(data);
      } catch (err) {
        console.log(err);
      }
    }
    fetchData();
  }, []);

  const deleteProvider = (e, id) => {
    e.preventDefault();
    axios.delete(`${URL}/delete/${id}`).then((res) => {
      setProviderList(
        providerList.filter((val) => {
          return val.id !== id;
        })
      );
      alert(`Delete id =${id} care provider successfully`);
    });
    setTimeout(function () {
      window.location.reload();
    }, 300);
  };

  return (
    <div className="provider-list">
      <div>
        <h1>Care Provider List</h1>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Care Provider ID
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Image
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Doctor First Name
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Doctor Last Name
                </TableCell>
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Doctor UserName
                </TableCell>
                {/* <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Admin
                </TableCell> */}
                <TableCell
                  align="center"
                  style={{ fontWeight: "700", color: "#230051" }}
                >
                  Edit / Delete
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {providerList &&
                providerList.map((p) => (
                  <TableRow key={p.careprovider_id}>
                    <TableCell align="center">{p.careprovider_id}</TableCell>
                    <TableCell align="center">
                      <img
                        src={p.careprovider_avatar}
                        className="w-6 border-2 border-white"
                        alt="Avatar"
                      />
                    </TableCell>
                    <TableCell align="center">
                      {p.careprovider_firstname}
                    </TableCell>
                    <TableCell align="center">
                      {p.careprovider_lastname}
                    </TableCell>
                    <TableCell align="center">
                      {p.careprovider_username}
                    </TableCell>
                    {/* <TableCell align="center">{p.is_admin}</TableCell> */}
                    <TableCell align="center">
                      <Link
                        to={`/admin/api/provider/update/${p.careprovider_id}`}
                      >
                        <Button
                          variant="contained"
                          color="primary"
                          style={{ margin: "8px" }}
                        >
                          Edit
                        </Button>
                      </Link>

                      <Button
                        variant="contained"
                        color="secondary"
                        onClick={(e, id) => {
                          deleteProvider(e, p.careprovider_id);
                        }}
                      >
                        Delete
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  );
}

export default ProviderList;
