import React, { useState, useEffect } from "react";
import axios from "axios";
import { TextField, Button } from "@material-ui/core";
import "./style.css";

import {
  TableBody,
  TableCell,
  TableContainer,
  Table,
  TableHead,
  TableRow,
} from "@material-ui/core";

import { Link } from "react-router-dom";

function ProviderSearch(props) {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [providerList, setProviderList] = useState([]);

  const URL = "http://localhost:5500/api/provider";
  const [findResult, setFindResult] = useState([]);
  const [findOne, setFindOne] = useState(false);

  useEffect(() => {
    async function fetchData() {
      try {
        let {
          data: { data },
        } = await axios.get(`${URL}`, {
          headers: {
            "Content-Type": "application/json",
          },
        });
        setProviderList(Array.from(data));
      } catch (err) {
        console.log(err);
      }
    }
    fetchData();
  }, []);

  const findProvider = (e) => {
    e.preventDefault();
    let searchData = {
      careprovider_firstname: firstName,
      careprovider_lastname: lastName,
    };

    try {
      axios.post(`${URL}/search`, searchData).then((res) => {
        let result = providerList.filter(
          (item) =>
            item.careprovider_firstname ===
              res.data[0]?.careprovider_firstname &&
            item.careprovider_lastname === res.data[0]?.careprovider_lastname
        );
        if (result) {
          setFindOne(true);
          setFindResult({ findResult: result[0] });

          console.log(setFindResult(result));
        } else {
          setFindOne(false);
        }
      });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };

  const deleteProvider = (e, id) => {
    e.preventDefault();
    axios.delete(`${URL}/delete/${id}`).then((res) => {
      setProviderList(
        providerList.filter((val) => {
          return val.id !== id;
        })
      );
      alert(`Delete id =${id} care provider successfully`);
    });
    setTimeout(function () {
      window.location.reload();
    }, 300);
  };

  return (
    <div className="container_searchProvider">
      <h1>Search a Care Provider's Info </h1>
      <form
        className="register_provider_form"
        onSubmit={(e, id) => findProvider(e, props.match.params.id)}
      >
        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          placeholder="Care provider first name"
          label="Input careprovider first name"
          variant="outlined"
          type="text"
          helperText="This field is required."
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          color="primary"
        />

        <TextField
          style={{ marginBottom: "30px" }}
          id="standard-full-width"
          label="Input careprovider last name"
          placeholder="care provider last name"
          variant="outlined"
          type="text"
          helperText="This field is required"
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        />

        <Button type="submit" variant="outlined" color="primary" boxShadow={3}>
          search
        </Button>
      </form>

      {findOne &&
        Array.from(findResult).map((p) => (
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell
                    align="center"
                    style={{ fontWeight: "700", color: "#230051" }}
                  >
                    Care Provider ID
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ fontWeight: "700", color: "#230051" }}
                  >
                    Image
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ fontWeight: "700", color: "#230051" }}
                  >
                    Doctor First Name
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ fontWeight: "700", color: "#230051" }}
                  >
                    Doctor Last Name
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{ fontWeight: "700", color: "#230051" }}
                  >
                    Doctor UserName
                  </TableCell>
                  {/* <TableCell
                    align="center"
                    style={{ fontWeight: "700", color: "#230051" }}
                  >
                    Admin
                  </TableCell> */}
                  <TableCell
                    align="center"
                    style={{ fontWeight: "700", color: "#230051" }}
                  >
                    Action
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow key={p.careprovider_id}>
                  <TableCell align="center">{p.careprovider_id}</TableCell>
                  <TableCell align="center">
                    <img
                      src={p.careprovider_avatar}
                      className="w-6 border-2 border-white"
                      alt="Avatar"
                    />
                  </TableCell>
                  <TableCell align="center">
                    {p.careprovider_firstname}
                  </TableCell>
                  <TableCell align="center">
                    {p.careprovider_lastname}
                  </TableCell>
                  <TableCell align="center">
                    {p.careprovider_username}
                  </TableCell>
                  {/* <TableCell align="center">{p.is_admin}</TableCell> */}
                  <TableCell align="center">
                    <Link
                      to={`/admin/api/provider/update/${p.careprovider_id}`}
                    >
                      <Button
                        variant="contained"
                        color="primary"
                        style={{ margin: "8px" }}
                      >
                        Edit
                      </Button>
                    </Link>

                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={(e, id) => {
                        deleteProvider(e, p.careprovider_id);
                      }}
                    >
                      Delete
                    </Button>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        ))}
    </div>
  );
}

export default ProviderSearch;
