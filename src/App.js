import React, { useContext } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { AuthProvider, AuthContext } from "./context/AuthContext";
import { FetchProvider } from "./context/FetchContext";
import PatientList from "./components/PatientList/PatientList";
import ProviderList from "./components/ProviderList/ProviderList";
import PatientMain from "./components/PatientProfile/PatientProfile";
import ProviderAdd from "./components/ProviderList/ProviderAdd";
import ProviderUpdate from "./components/ProviderList/ProviderUpdate";
import ProviderSearch from "./components/ProviderList/ProviderSearch";
import PatientAdd from "./components/PatientList/PatientAdd";
import PatientUpdate from "./components/PatientList/PatientUpdate";
import PatientSearch from "./components/PatientList/PatientSearch";
import AllergyAdd from "./components/Allergy/AlleryAdd";
import AllergyList from "./components/Allergy/AlleryList";
import AllergyUpdate from "./components/Allergy/AllergyUpdate";
import ImmunizationAdd from "./components/Immunization/ImmunizationAdd";
import ImmunizationList from "./components/Immunization/ImmunizationList";
import ImmunizationUpdate from "./components/Immunization/ImmunizationUpdate";
import MedicalHistoryAdd from "./components/MedicalHistory/MedicalHistoryAdd";
import MedicalHistoryList from "./components/MedicalHistory/MedicalHistoryList";
import MedicalHistoryUpdate from "./components/MedicalHistory/MedicalHistoryUpdate";
import RadiologyAdd from "./components/Radiology/RadiologyAdd";
import RadiologyList from "./components/Radiology/RadiologyList";
import RadiologyUpdate from "./components/Radiology/RadiologyUpdate";
import BillingAdd from "./components/Billing/BillingAdd";
import BillingList from "./components/Billing/BillingList";
import BillingUpdate from "./components/Billing/BillingUpdate";
import TestingList from "./components/LabTest/TestingList";
import TestingAdd from "./components/LabTest/TestingAdd";
import TestingUpdate from "./components/LabTest/TestingUpdate";
import ProviderPatientSearch from "./components/ProviderPatientSearch/ProviderPatientSearch";
import PatientNoteAdd from "./components/MainPagePatientNote/PatientNoteAdd";
import PatientNoteUpdate from "./components/MainPagePatientNote/PatientNoteUpdate";
import MainPagePatientNote from "./components/MainPagePatientNote/MainPagePatientNote";
import Login from "./components/Login/Login";
import SignUp from "./components/SignUp/SignUp";
import FourOFour from "./components/FourOFour";
import PatientProfile from "./components/PatientProfile/PatientProfile";
import AppShell from "./AppShell";


function App() {
  return (
    <div className="App">   
      <Router>
        <AuthProvider>
          <FetchProvider>        
            <Switch>
              <AppShell>
                <Route path="/patient/search" exact component={ProviderPatientSearch} /> 
                <Route path="/admin/api/provider" exact component={ProviderList} />
                <Route path="admin/api/provider/delete/:id" exact component={ProviderList} />              
                <Route path="/admin/api/providerAdd" exact component={ProviderAdd} />
                <Route path="/admin/api/provider/update/:id" exact component={ProviderUpdate} />            
                <Route path="/admin/api/provider/search" exact component={ProviderSearch} />

                <Route path="/api/admin/patient/search" exact component={PatientSearch} />
                <Route path="/api/admin/patientList/:id" exact component={PatientMain} />
                <Route path="/api/admin/patientList" exact component={PatientList} />
                <Route path="/api/admin/patientAdd" exact component={PatientAdd} />
                <Route path="/api/admin/patient/update/:id" exact component={PatientUpdate} />
                
                <Route path="/api/patient/profile/:id" exact component={PatientProfile} />
                <Route path="/api/patient/allergy/add" exact component={AllergyAdd} />
                <Route path="/api/patient/allergy/list/:healthID" exact component={AllergyList} />
                <Route path="/api/patient/allergy/update/:id" exact component={AllergyUpdate} />

                <Route path="/api/patient/immunization/add" exact component={ImmunizationAdd} />
                <Route path="/api/patient/immunization/list/:healthID" exact component={ImmunizationList} />
                <Route path="/api/patient/immunization/update/:id" exact component={ImmunizationUpdate} />

                <Route path="/api/patient/medical/add" exact component={MedicalHistoryAdd} />
                <Route path="/api/patient/medical/list/:healthID" exact component={MedicalHistoryList} />
                <Route path="/api/patient/medical/update/:id" exact component={MedicalHistoryUpdate} />

                <Route path="/api/patient/radiology/radiologyAdd" exact component={RadiologyAdd} />
                <Route path="/api/patient/radiology/radiologyList/:healthID" exact component={RadiologyList} />
                <Route path="/api/patient/radiology/update/:id" exact component={RadiologyUpdate} />

                <Route path="/api/patient/billing/billingAdd" exact component={BillingAdd} />
                <Route path="/api/patient/billing/billingList/:healthID" exact component={BillingList} />
                <Route path="/api/patient/billing/update/:id" exact component={BillingUpdate} />

                <Route path="/api/patient/testing/testingAdd" exact component={TestingAdd} />
                <Route path="/api/patient/testing/testingList/:healthID" exact component={TestingList} />
                <Route path="/api/patient/testing/update/:id" exact component={TestingUpdate} /> 
                <Route path="/api/patient/note/add" exact component={PatientNoteAdd} />
                <Route path="/api/patient/note/:id" exact component={MainPagePatientNote} />
                <Route path="/api/patient/note/update/:id" exact component={PatientNoteUpdate} />

                <Route path="/" exact component={Login} />
                <Route path="/signup" exact component={SignUp} />
              </AppShell>            
              {/* <Route component={FourOFour} /> */}
            </Switch>          
          </FetchProvider>
        </AuthProvider>
        
      </Router>
    </div>
  );
}

export default App;
