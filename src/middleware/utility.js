import * as jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

const createToken = user => {
  // Sign the JWT
  if (!user.user_role) {
    throw new Error('No user role specified');
  }
  return jwt.sign(
    {      
      email: user.user_email,
      role: user.user_role,
      iss: 'api.emrsystem',
      aud: 'api.emrsystem'
    },
    process.env.JWT_SECRET,
    { algorithm: 'HS256', expiresIn: '1h' }
  );
};

const hashPassword = password => {
  return new Promise((resolve, reject) => {
    // Generate a salt at level 10 strength
    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        reject(err);
      }
      bcrypt.hash(password, salt, (err, hash) => {
        if (err) {
          reject(err);
        }
        resolve(hash);
      });
    });
  });
};


const verifyPassword = (
  passwordAttempt,
  hashedPassword
) => {
  return bcrypt.compare(passwordAttempt, hashedPassword);
};

const requireAdmin = (req, res, next) => {
  if (!req.user) {
    return res.status(401).json({
      message: 'There was a problem authorizing the request'
    });
  }
  if (req.user.role !== 'admin') {
    return res
      .status(401)
      .json({ message: 'Insufficient role' });
  }
  next();
};

export {
  createToken,
  hashPassword,
  verifyPassword,
  requireAdmin
};
