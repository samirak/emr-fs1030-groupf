CREATE TABLE IF NOT EXISTS `user`
(
  `user_firstName` VARCHAR(250) NOT NULL,
  `user_lastName` VARCHAR(250) NOT NULL,
  `user_email` VARCHAR(250) NOT NULL,
  `user_password` TEXT NOT NULL,
  `user_role` VARCHAR(50) NOT NULL DEFAULT 'user',
  PRIMARY KEY (`user_email`)
);


SELECT * FROM user;
DROP TABLE user;

