CREATE TABLE IF NOT EXISTS `patient_notes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `patient_healthcard` VARCHAR(20) NOT NULL,
  `patient_note` TEXT,
  `date_created` VARCHAR(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_healthcard` (`patient_healthcard`),
  CONSTRAINT `patient_notes` FOREIGN KEY (`patient_healthcard`) REFERENCES `patient_profile` (`patient_healthcard`)
);

INSERT INTO `patient_notes` VALUES (0,'222-222-2222','Patient is suffering of sleeping issue.','2020-08-15');


SELECT * FROM patient_notes;
