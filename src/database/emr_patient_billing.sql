CREATE TABLE IF NOT EXISTS `patient_billing` (
  `patient_healthcard` VARCHAR(20) NOT NULL,
  `transaction_type` VARCHAR(100) DEFAULT 'credit card',
  `amount` INT,
  `reference_number` INT DEFAULT '123456789',
  `status` VARCHAR(40) NOT NULL DEFAULT '', 
  `date_completed` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`reference_number`),
  CONSTRAINT `patient_billing` FOREIGN KEY (`patient_healthcard`) REFERENCES `patient_profile` (`patient_healthcard`)
);


INSERT INTO `patient_billing` VALUES ('222-222-2222','credit card','250',123456789,'success','2019-08-18');


DROP TABLE patient_billing;
SELECT * FROM patient_billing;