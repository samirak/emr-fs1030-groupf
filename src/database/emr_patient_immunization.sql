CREATE TABLE IF NOT EXISTS `immunization`
(
  `immunization_id` INT NOT NULL AUTO_INCREMENT,
  `immunization_name` VARCHAR(150) NOT NULL,
  `immunization_dose` VARCHAR(25),
  `immunization_desc` LONGTEXT,
  `immunization_date_completed` TEXT NOT NULL,
  `immunization_provider` VARCHAR(150) NOT NULL,
  `patient_healthcard` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`immunization_id`, `patient_healthcard`),
  CONSTRAINT `fk_patient_immunization` FOREIGN KEY (`patient_healthcard`) REFERENCES `patient_profile` (`patient_healthcard`)
);

INSERT INTO `immunization` VALUES ('1','Moderna','0.5 ml','Coronavirus','2021-04-03','Dr.Amit Pachan','222-222-2222'),('2','Pfizer-BioNTech','0.5 ml','COVID-19 vaccine','2021-03-24','Dr.Richard','111-111-1111');

SELECT * FROM immunization;
DROP TABLE immunization;
