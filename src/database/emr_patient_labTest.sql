CREATE TABLE IF NOT EXISTS `lab_test` (
  `lab_id` int NOT NULL AUTO_INCREMENT,
  `patient_healthcard` VARCHAR(20) NOT NULL,
  `test_name` VARCHAR(100) NOT NULL DEFAULT '',
  `result_desc` TEXT,
  `positive_negative` VARCHAR(20) NOT NULL DEFAULT '',
  `date_completed` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`lab_id`,`patient_healthcard`),
  CONSTRAINT `lab_test` FOREIGN KEY (`patient_healthcard`) REFERENCES `patient_profile` (`patient_healthcard`)
);


INSERT INTO `lab_test` VALUES ('0','222-222-2222','Blood test','In normal scope','negative','2019-08-15');


SELECT * FROM lab_test;
DROP TABLE lab_test;
