CREATE TABLE IF NOT EXISTS `patient_radiology` (
  `radio_id` int NOT NULL AUTO_INCREMENT,
  `patient_healthcard` VARCHAR(20) NOT NULL,
  `exam_type` VARCHAR(150) NOT NULL DEFAULT '',
  `result_desc` TEXT,
  `image` TEXT,
  `date_completed` DATE DEFAULT NULL, 
  PRIMARY KEY (`radio_id`,`patient_healthcard`),
  CONSTRAINT `patient_radiology` FOREIGN KEY (`patient_healthcard`) REFERENCES `patient_profile` (`patient_healthcard`)
); 

INSERT INTO `patient_radiology` VALUES ('0','222-222-2222','CT Scan','Negative','https://cdn-prod.medicalnewstoday.com/content/images/articles/153/153201/a-ct-scan-shows.jpg','2005-10-25');


SELECT * FROM patient_radiology;
DROP TABLE patient_radiology;
