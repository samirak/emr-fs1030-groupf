CREATE TABLE IF NOT EXISTS care_provider (
  careprovider_id int(20) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  careprovider_firstname varchar(100) NOT NULL DEFAULT '',
  careprovider_lastname varchar(100) NOT NULL DEFAULT '',
  careprovider_username varchar(10) NOT NULL DEFAULT '',
  careprovider_avatar TEXT,
  careprovider_password text NOT NULL, 
  is_admin tinyint(1) NOT NULL DEFAULT '0'
);

INSERT INTO care_provider VALUES (1,'Tom','Smith','tom','https://lh3.googleusercontent.com/proxy/QJSvf5pGaAvPF0ioaDewu-yS3389XNWWy5UOJjV6nan2shJR-lpQ6K7JRECdJpBnFODtXo5VEIg1u38aMzd1OEXT79EvBdXSJtq87H2zTOQq2UlJPX2ODQ','$2b$10$aCaNGKDk1xeWwKuqAyMK9Om9tn.',0),
(2,'LILI','Black','lili','https://lh3.googleusercontent.com/proxy/QJSvf5pGaAvPF0ioaDewu-yS3389XNWWy5UOJjV6nan2shJR-lpQ6K7JRECdJpBnFODtXo5VEIg1u38aMzd1OEXT79EvBdXSJtq87H2zTOQq2UlJPX2ODQ','$2b$10$aCaNGKDk1xeWwKuqAyMK9Om9tn.',1);

SELECT * FROM  care_provider;
DROP TABLE care_provider;
