CREATE TABLE IF NOT EXISTS `medical_history` 
(
  `medical_history_id` INT NOT NULL AUTO_INCREMENT,
  `medical_history_medication` VARCHAR(150) NOT NULL,  
  `medical_history_condition` VARCHAR(150) NOT NULL,
  `medical_history_dose` VARCHAR(25) NOT NULL,
  `medical_history_date_prescribed` TEXT NOT NULL,
  `medical_history_provider` VARCHAR(150) NOT NULL,
  `medical_history_decs` LONGTEXT,
  `patient_healthcard` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`medical_history_id`, `patient_healthcard`),
  CONSTRAINT `fk_patient_medication` FOREIGN KEY (`patient_healthcard`) REFERENCES `patient_profile` (`patient_healthcard`)
); 


INSERT INTO `medical_history` VALUES ('1','Aspirin','Heart surgery','81 mg','2010-10-01','Dr.Amit Pachan', 'high colestrol family history', '111-111-1111'),('2','xxx ashma medication','Asthma','4 puffs','2001-01-11','Dr.Younge','No family history for Asthma','222-222-2222');

SELECT * FROM medical_history;
DROP TABLE medical_history;
