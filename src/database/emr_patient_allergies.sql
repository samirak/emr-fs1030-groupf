CREATE TABLE IF NOT EXISTS `allergy`
(
  `allergy_id` INT NOT NULL AUTO_INCREMENT,
  `allergy_name` VARCHAR(150) NOT NULL,
  `allergy_status` VARCHAR(25),
  `allergy_severity` VARCHAR(25),
  `allergy_desc` LONGTEXT,
  `patient_healthcard` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`allergy_id`, `patient_healthcard`),
  CONSTRAINT `fk_patient_allergy` FOREIGN KEY (`patient_healthcard`) REFERENCES `patient_profile` (`patient_healthcard`)
);

INSERT INTO `allergy` VALUES ('1','Drug','Active','Severe','Patient consumes yyy medicine', '111-111-1111'),('2','Food','Active','Mild','xxx medicine prescribed','222-222-2222'),('3','Pollen','Inactive','minor','Patient consumes zzz medicine', '333-333-3333');

SELECT * FROM allergy;
DROP TABLE allergy;

