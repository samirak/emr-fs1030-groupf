CREATE TABLE IF NOT EXISTS `patient_profile` (
  `patient_healthcard` VARCHAR(20) NOT NULL DEFAULT '',
  `patient_firstname` VARCHAR(100) NOT NULL DEFAULT '',
  `patient_lastname` VARCHAR(100) NOT NULL DEFAULT '',
  `patient_DOB` VARCHAR(45) NOT NULL DEFAULT '',
  `patient_phone` VARCHAR(45) NOT NULL DEFAULT '',
  `patient_street`  VARCHAR(100) NOT NULL DEFAULT '',
  `patient_city` VARCHAR(100) NOT NULL DEFAULT '',
  `patient_province` VARCHAR(100) NOT NULL DEFAULT '',
  `patient_postalcode` VARCHAR(30) NOT NULL DEFAULT '',
  `patient_gender` VARCHAR(30) NOT NULL DEFAULT '',
  `patient_email` VARCHAR(40) NOT NULL DEFAULT '',
  `patient_marital` VARCHAR(100) NOT NULL DEFAULT '',
  `patient_insurance` INT(50) NOT NULL DEFAULT '123456789',
  PRIMARY KEY (`patient_healthcard`)
);

DESCRIBE patient_profile;


INSERT INTO `patient_profile` VALUES ('222-222-2222','Sara','Smith','1980-03-25','202-202-2020','123 Street','Winnipeg','MB','R2P 3S9','Female','sara@gmail.com','married','123246789');
SELECT * FROM patient_profile;
DROP TABLE patient_profile;

SELECT * FROM patient_profile WHERE patient_healthcard='222-222-2222';