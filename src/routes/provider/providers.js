import express from "express";
import db from "../../../server.js";
import bcrypt from "bcrypt";
import { stringify } from "flatted";
const providerRoute = express.Router();
const saltRounds = 10;


//get all providers list
providerRoute.get("/", async (req, res) => {
  try {
    await db.query(
      "SELECT careprovider_id, careprovider_firstname, careprovider_lastname, careprovider_username,careprovider_avatar, is_admin FROM care_provider",
      (err, data) => {
        if (err) {
          console.log(err);
          return res.status(400).json({ message: err });
        } else {
          console.log("get all providers data successfully.");
          res.status(200).json({
            data,
          });
        }
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
});

//get by id
providerRoute.get("/:id", (req, res) => {
  try {
    let id = req.params.id;
    db.query(
      `SELECT * FROM care_provider WHERE careprovider_id=${id}`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          res.json(result);
          console.log("get id data successfully.");
        }
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
});

// search one doctor by his first name and last name
providerRoute.post("/search", (req, res) => {
  try {
    const careprovider_firstname = req.body.careprovider_firstname;
    const careprovider_lastname = req.body.careprovider_lastname;

    db.query(
      "SELECT careprovider_id, careprovider_firstname, careprovider_lastname, careprovider_username,careprovider_avatar, is_admin FROM care_provider WHERE careprovider_firstname =? AND careprovider_lastname =?",
      [careprovider_firstname, careprovider_lastname],
      (err, data) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log(`find the care provider!`);
          res.status(200).json(data);
        }
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
});

//Create a new care provider
providerRoute.post("/create", async (req, res) => {
  try {
    const careprovider_firstname = req.body.careprovider_firstname;
    const careprovider_lastname = req.body.careprovider_lastname;
    const careprovider_username = req.body.careprovider_username;
    const careprovider_password = req.body.careprovider_password;
    const careprovider_avatar = req.body.careprovider_avatar;
    const is_admin = req.body.is_admin;

    //hash password
    const careprovider_hashedPW = await bcrypt.hash(
      careprovider_password,
      saltRounds
    );

    let newProvider = await db.query(
      "INSERT INTO care_provider(careprovider_firstname, careprovider_lastname, careprovider_username, careprovider_password,careprovider_avatar, is_admin) VALUES (?,?,?,?,?,?)",
      [
        careprovider_firstname,
        careprovider_lastname,
        careprovider_username,
        careprovider_hashedPW,
        careprovider_avatar,
        is_admin,
      ]
    );
    let newDoctor = stringify(newProvider);
    res.status(200).json({ newDoctor });
    console.log("Add a new care provider successfully!");
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

//update a new doctor's info
providerRoute.put("/update/:id", async (req, res) => {
  try {
    const careprovider_firstname = req.body.careprovider_firstname;
    const careprovider_lastname = req.body.careprovider_lastname;
    const careprovider_username = req.body.careprovider_username;
    const careprovider_password = req.body.careprovider_password;
    const careprovider_avatar = req.body.careprovider_avatar;
    const is_admin = req.body.is_admin;
    const id = req.params.id;

    //hash updated password
    let careprovider_hashedPW = await bcrypt.hash(
      careprovider_password,
      saltRounds
    );

    // let updateDoctor = stringify(updateProvider);
    let updateProvider = db.query(
      "UPDATE care_provider SET careprovider_firstname=?, careprovider_lastname=?, careprovider_username=?, careprovider_password=?, careprovider_avatar=?,is_admin=? WHERE careprovider_id=?",
      [
        careprovider_firstname,
        careprovider_lastname,
        careprovider_username,
        careprovider_hashedPW,
        careprovider_avatar,
        is_admin,
        id,
      ]
    );

    let newDoctor = stringify(updateProvider);
    res.status(200).json(newDoctor);
    console.log("Update a care provider successfully!");
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

// delete one care provider
providerRoute.delete("/delete/:id", (req, res) => {
  const id = req.params.id;
  db.query(
    "DELETE FROM care_provider WHERE  careprovider_id=?",
    id,
    (err, result) => {
      if (err) {
        return res.status(400).json({ message: err });
      } else {
        console.log(`delete provider at his ID=${id} item successfully.`);
        res.send(result);
      }
    }
  );
});

export default providerRoute;
