import express from "express";
import providersRoute from "./provider/providers";
import patientAdminRoute from "./patient/patientAdminRoute";
import patientAllergy from "./patient/patientAllergyRoute";
import patientImmunization from "./patient/patientImmunizationRoute";
import patientMedicalHistory from "./patient/patientMedicalHistoryRoute";
import patientRadiologyRoute from "./patient/patientRadiologyRoute";
import patientBillingRoute from "./patient/patientBillingRoute";
import patientTestingRoute from "./patient/patientTestingRoute";
import patientNote from "./patient/patientNoteRoute";
import userRoute from "./user/userRoute";

let emrRouter = express.Router();

// Care provider list
emrRouter.use("/provider", providersRoute);

// Admin control patient CRUD
emrRouter.use("/admin/patient", patientAdminRoute);

// Patient allergy CRUD
emrRouter.use("/patient/allergy", patientAllergy);

// Patient immunization CRUD
emrRouter.use("/patient/immunization", patientImmunization);

// Patient medical history CRUD
emrRouter.use("/patient/medical", patientMedicalHistory);

// Patient Radiology CRUD
emrRouter.use("/patient/radiology", patientRadiologyRoute);

// Patient Billing CRUD
emrRouter.use("/patient/billing", patientBillingRoute);

// Patient Testing CRUD
emrRouter.use("/patient/testing", patientTestingRoute);

// Patient Notes CRUD
emrRouter.use("/patient/note", patientNote);

// User
emrRouter.use("/user", userRoute);

export default emrRouter;
