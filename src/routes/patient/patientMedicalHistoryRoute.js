import express from "express";
import db from "../../../server";

const patientMedicalHistory = express.Router();

// Getting all medical history records of all patients
patientMedicalHistory.get("/", (req, res) => {
  try {
    db.query("SELECT * FROM medical_history", (err, result) => {
      if (err) {
        console.log(err);
        throw err;
      } else {
        console.log(result);
        res.status(200).json({ result });
        console.log(
          "Retrieved medical history data of all patients successfully."
        );
      }
    });
  } catch (err) {
    throw err;
  }
});

// Getting all medical records of a patient by health card id
patientMedicalHistory.get("/:healthID", (req, res) => {
  try {
    const healthID = req.params.healthID;

    db.query(
      `SELECT * FROM medical_history WHERE patient_healthcard = '${healthID}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({ result });
          console.log(
            `Retrieved medical history data of patient with healthcard number ${healthID} successfully.`
          );
        }
      }
    );
  } catch (err) {
    console.error("Error in getting medical history record route!", err);
    //return next(err);
  }
});

// Getting one medical record of a patient by medical history id
patientMedicalHistory.get("/record/:id", (req, res) => {
  try {
    const id = req.params.id;

    db.query(
      `SELECT * FROM medical_history WHERE medical_history_id = '${id}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({ result });
          console.log(
            `Retrieved medical history data of patient successfully.`
          );
        }
      }
    );
  } catch (err) {
    console.error("Error in getting medical history record route!", err);
    //return next(err);
  }
});

// Adding a new medical history record to db
patientMedicalHistory.post("/create", (req, res) => {
  try {
    let sqlQuery =
      "INSERT INTO medical_history (medical_history_medication, medical_history_condition, medical_history_dose, medical_history_date_prescribed, medical_history_provider, medical_history_decs, patient_healthcard) VALUES (?,?,?,?,?,?,?)";

    db.query(
      sqlQuery,
      [
        req.body.medical_history_medication,
        req.body.medical_history_condition,
        req.body.medical_history_dose,
        req.body.medical_history_date_prescribed,
        req.body.medical_history_provider,
        req.body.medical_history_decs,
        req.body.patient_healthcard,
      ],
      (err, result) => {
        if (err) {
          console.log(err);
          return res.status(400).json({ message: err });
        } else {
          console.log("Created a new medical history record successfully!");
          res.status(201).json(result);
        }
      }
    );
  } catch (err) {
    console.error("Error in creating new medical history record route!", err);
    //return next(err);
  }
});

// Updating an existing medical history record
patientMedicalHistory.put("/update/:id", (req, res) => {
  try {
    const id = req.params.id;
    let sqlQuery = `UPDATE medical_history SET medical_history_medication=?, medical_history_condition=?, medical_history_dose=?, medical_history_date_prescribed=?, medical_history_provider=?, medical_history_decs=? WHERE medical_history_id='${id}'`;
    db.query(
      sqlQuery,
      [
        req.body.medical_history_medication,
        req.body.medical_history_condition,
        req.body.medical_history_dose,
        req.body.medical_history_date_prescribed,
        req.body.medical_history_provider,
        req.body.medical_history_decs,
      ],
      (err, result) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log("Updated the medical history record successfully!");
          res.status(200).json(result);
        }
      }
    );
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

// Deleting an existing medical history record
patientMedicalHistory.delete("/delete/:id", (req, res) => {
  try {
    const id = req.params.id;

    db.query(
      `DELETE FROM medical_history WHERE medical_history_id = '${id}'`,
      (err, result) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log(result);
          res.status(200).send(result);
        }
      }
    );
  } catch (err) {
    console.error("Error in deleting medical history record route!", err);
    //return next(err);
  }
});

export default patientMedicalHistory;
