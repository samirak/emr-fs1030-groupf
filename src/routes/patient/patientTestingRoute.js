import express from "express";
import db from "../../../server.js";
import { stringify } from "flatted";

const patientTestingRoute = express.Router();

patientTestingRoute.get("/", (req, res) => {
  try {
    db.query(
      "SELECT lab_id, patient_healthcard, test_name, result_desc, positive_negative, date_completed FROM lab_test ",
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({ result });
          console.log("Retrieved testing data of all patients successfully.");
        }
      }
    );
  } catch (err) {
    throw err;
  }
});

//Get a patient's all lab tests listed with health card ID
patientTestingRoute.get("/:healthID", (req, res) => {
  try {
    let healthID = req.params.healthID;

    db.query(
      `SELECT * FROM lab_test WHERE patient_healthcard = '${healthID}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({
            result,
          });
          console.log(
            `Successfully retrieved lab test list for patient ${healthID}.`
          );
        }
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
});

//Get a patient's lab test listed with lab ID
patientTestingRoute.get("/record/:id", (req, res) => {
  try {
    let id = req.params.id;

    db.query(
      `SELECT * FROM lab_test WHERE lab_id = '${id}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({
            result,
          });
          console.log(
            `Successfully retrieved lab test with id: ${id}.`
          );
        }
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
});

//Create New Test For a Patient
patientTestingRoute.post("/create", async (req, res) => {
  try {
    const {
      patient_healthcard,
      test_name,
      result_desc,
      positive_negative,
      date_completed,
    } = req.body;

    let newTest = await db.query(
      "INSERT INTO lab_test(patient_healthcard, test_name, result_desc, positive_negative, date_completed) VALUES (?,?,?,?,?)",
      [
        patient_healthcard,
        test_name,
        result_desc,
        positive_negative,
        date_completed,
      ]
    );

    let newT = stringify(newTest);
    res.status(200).json(newT);

    console.log(`Test created for patient`);
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

//Update A Lab Test For a Patient

patientTestingRoute.put("/update/:id", async (req, res) => {
  try {
    const {
      test_name,
      result_desc,
      positive_negative,
      date_completed,
    } = req.body;
    const id = req.params.id;

    let updatePatientTesting = db.query(
      `UPDATE lab_test SET test_name=?, result_desc=?, positive_negative=?, date_completed=? WHERE lab_id=?`,
      [test_name, result_desc, positive_negative, date_completed, id]
    );

    let upPatientTest = stringify(updatePatientTesting);
    res.status(200).json(upPatientTest);
    console.log(`Updated test number ${id}!`);
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

//Delete Test By Lab ID Number

patientTestingRoute.delete("/delete/:id", (req, res) => {
  const id = req.params.id;
  db.query("DELETE FROM lab_test WHERE lab_id=?", id, (err, result) => {
    if (err) {
      return res.status(400).json({ message: err });
    } else {
      console.log(`Deleted lab test with ID: ${id}.`);
      res.send(result);
    }
  });
});

export default patientTestingRoute;
