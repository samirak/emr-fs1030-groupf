import express from "express";
import db from "../../../server";

const patientImmunization = express.Router();

// Getting all immunization records of all patients
patientImmunization.get("/", (req, res) => {
  try {
    db.query(
      "SELECT immunization_id, immunization_name, immunization_dose, immunization_desc, immunization_date_completed, immunization_provider, patient_healthcard FROM immunization",
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({ result });
          console.log(
            "Retrieved immunization data of all patients successfully."
          );
        }
      }
    );
  } catch (err) {
    throw err;
  }
});

// Getting all immunization records of a patient by healthcard id
patientImmunization.get("/:healthID", (req, res) => {
  try {
    const healthID = req.params.healthID;

    db.query(
      `SELECT * FROM immunization WHERE patient_healthcard = '${healthID}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({ result });
          console.log(
            `Retrieved immunization data of patient with healthcard number ${healthID} successfully.`
          );
        }
      }
    );
  } catch (err) {
    console.error("Error in getting immunization record route!", err);
    //return next(err);
  }
});

// Getting all immunization records of a patient by immunization id
patientImmunization.get("/record/:id", (req, res) => {
  try {
    const id = req.params.id;

    db.query(
      `SELECT * FROM immunization WHERE immunization_id = '${id}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({ result });
          console.log(
            `Retrieved immunization record of patient successfully.`
          );
        }
      }
    );
  } catch (err) {
    console.error("Error in getting immunization record route!", err);
    //return next(err);
  }
});

// Adding a new immunization record to db
patientImmunization.post("/create", async (req, res) => {
  try {
    let sqlQuery =
      "INSERT INTO immunization (immunization_name, immunization_dose, immunization_desc, immunization_date_completed, immunization_provider, patient_healthcard) VALUES (?,?,?,?,?,?)";

    db.query(
      sqlQuery,
      [
        req.body.immunization_name,
        req.body.immunization_dose,
        req.body.immunization_desc,
        req.body.immunization_date_completed,
        req.body.immunization_provider,
        req.body.patient_healthcard,
      ],
      (err, result) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log("Created a new immunization record successfully!");
          res.status(201).json(result);
        }
      }
    );
  } catch (err) {
    console.error("Error in creating new immunization record route!", err);
    //return next(err);
  }
});

// Updating an existing patient's immunization info
patientImmunization.put("/update/:id", async (req, res) => {
  try {
    db.query(
      `UPDATE immunization SET immunization_name=?, immunization_dose=?, immunization_desc=?, immunization_date_completed=?, immunization_provider=? WHERE immunization_id=?`,
      [
        req.body.immunization_name,
        req.body.immunization_dose,
        req.body.immunization_desc,
        req.body.immunization_date_completed,
        req.body.immunization_provider,
        req.params.id,
      ],
      (err, result) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log("Updated the immunization info successfully!");
          //let upImmune = stringify(result);
          res.status(200).json(result);
        }
      }
    );
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

// Deleting an existing immunization record
patientImmunization.delete("/delete/:id", (req, res) => {
  try {
    const id = req.params.id;

    db.query(
      "DELETE FROM immunization WHERE immunization_id=?",
      id,
      (err, result) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log(`Deleted the immunization record successfully.`);
          res.send(result);
        }
      }
    );
  } catch (err) {
    console.error("Error in deleting immunization record route!", err);
    //return next(err);
  }
});

export default patientImmunization;
