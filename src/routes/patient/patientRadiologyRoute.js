import express from "express";
import db from "../../../server.js";
import { stringify } from "flatted";

const patientRadiologyRoute = express.Router();

patientRadiologyRoute.get("/", (req, res) => {
    try {
      db.query(
        "SELECT radio_id, patient_healthcard, exam_type, result_desc, image, date_completed FROM patient_radiology ",
        (err, result) => {
          if (err) {
            console.log(err);
            throw err;
          } else {
            console.log(result);
            res.status(200).json({ result });
            console.log("Retrieved radiology data of all patients successfully.");
          }
        }
      );
    } catch (err) {
      throw err;
    }
  });
  
//Get a patient's radiology with health card ID
patientRadiologyRoute.get("/:healthID", (req, res) => {
  try {
    let healthID = req.params.healthID;
    console.log(healthID);

    db.query(
      `SELECT * FROM patient_radiology WHERE patient_healthcard = '${healthID}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({
            result,
          });
          console.log(`successfully retrieved radiology test history for patient with healthcard: ${healthID}.`);
        }
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
});
  

//Get a patient's radiology with radiology ID
patientRadiologyRoute.get("/record/:id", (req, res) => {
    try {
      let id = req.params.id;
  
      db.query(
        `SELECT * FROM patient_radiology WHERE radio_id = '${id}'`,
        (err, result) => {
          if (err) {
            console.log(err);
            throw err;
          } else {
            console.log(result);
            res.status(200).json({
              result,
            });
            console.log(`successfully retrieved radiology test history of id: ${id}.`);
          }
        }
      );
    } catch (err) {
      console.log(err);
      throw err;
    }
  });

  //Create Radiology Test For a Patient
  patientRadiologyRoute.post("/create", async (req, res) => {
    try {
      const {
        patient_healthcard,
        exam_type,
        result_desc,
        image,
        date_completed,
      } = req.body;
  
      let newRadiology = await db.query(
        "INSERT INTO patient_radiology(patient_healthcard, exam_type, result_desc, image, date_completed) VALUES (?,?,?,?,?)",
        [
        patient_healthcard,
        exam_type,
        result_desc,
        image,
        date_completed,
        ]
      );
  
      let newR = stringify(newRadiology);
      res.status(200).json(newR);
  
      console.log(`Radiology test created for patient`);
    } catch (err) {
      console.error(err.message);
      res.status(500).send({ message: err });
    }
  });

  //Update Radiology For a Patient

  patientRadiologyRoute.put("/update/:id", async (req, res) => {
    try {
      const {
        exam_type,
        result_desc,
        image,
        date_completed,
      } = req.body;
      const id = req.params.id;
  
      let updatePatientRadiology = db.query(
        `UPDATE patient_radiology SET exam_type=?, result_desc=?, image=?, date_completed=? WHERE radio_id=?`,
        [
        
        exam_type,
        result_desc,
        image,
        date_completed,
        id,
        ]
      );
  
      let upPatientRad = stringify(updatePatientRadiology);
      res.status(200).json(upPatientRad);
      console.log(`Updated Radiology for radiology report: ${id}!`);
    } catch (err) {
      console.error(err.message);
      res.status(500).send({ message: err });
    }
  });
  
  //Delete Radiology by Radio ID

  patientRadiologyRoute.delete("/delete/:id", (req, res) => {
    const id = req.params.id;
    db.query(
      `DELETE FROM patient_radiology WHERE radio_id=?`,
      id,
      (err, result) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log(
            `Deleted Radiology Report With ID ${id}.`
          );
          res.send(result);
        }
      }
    );
  
    
  });
  
export default patientRadiologyRoute;