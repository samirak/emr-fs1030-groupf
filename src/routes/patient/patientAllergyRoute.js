import express from "express";
import db from "../../../server";

const patientAllergy = express.Router();

// Getting all allergies of all patients
patientAllergy.get("/", (req, res) => {
  try {
    db.query(
      "SELECT allergy_id, allergy_name, allergy_status, allergy_severity, allergy_desc, patient_healthcard FROM allergy",
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({ result });
          console.log("Retrieved allergy data of all patients successfully.");
        }
      }
    );
  } catch (err) {
    throw err;
  }
});

// Getting all allergies of a patient by healthcard id
patientAllergy.get("/:healthID", (req, res) => {
  try {
    const healthID = req.params.healthID;

    db.query(
      `SELECT * FROM allergy WHERE patient_healthcard = '${healthID}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({ result });
          console.log(
            `Retrieved allergy information of patient with healthcard number ${healthID} successfully.`
          );
        }
      }
    );
  } catch (err) {
    throw err;
  }
});

// Getting a specific allergy of a patient by allery id
patientAllergy.get("/record/:id", (req, res) => {
  try {
    const id = req.params.id;

    db.query(
      `SELECT * FROM allergy WHERE allergy_id = '${id}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({ result });
          console.log(
            `Retrieved allergy information successfully.`
          );
        }
      }
    );
  } catch (err) {
    throw err;
  }
});

// Adding a new allergy record to db
patientAllergy.post("/create", async (req, res) => {
  try {
    let sqlQuery =
      "INSERT INTO allergy (allergy_name, allergy_status, allergy_severity, allergy_desc, patient_healthcard) VALUES (?,?,?,?,?)";

    db.query(
      sqlQuery,
      [
        req.body.allergy_name,
        req.body.allergy_status,
        req.body.allergy_severity,
        req.body.allergy_desc,
        req.body.patient_healthcard,
      ],
      (err, data) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log("Posted the new allergy!");
          res.status(201).json(data);
        }
      }
    );
  } catch (err) {
    console.error("Error in creating new allergy route!", err);
    //return next(err);
  }
});

// Updating an existing patient's allergy info
patientAllergy.put("/update/:id", async (req, res) => {
  try {
    db.query(
      `UPDATE allergy SET allergy_name=?, allergy_status=?, allergy_severity=?, allergy_desc=? WHERE allergy_id=?`,
      [
        req.body.allergy_name,
        req.body.allergy_status,
        req.body.allergy_severity,
        req.body.allergy_desc,
        req.params.id,
      ],
      (err, data) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log("Updated the allery info successfully!");
          res.status(200).json(data);
        }
      }
    );
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

// Deleting an existing allergy record
patientAllergy.delete("/delete/:id", (req, res) => {
  const id = req.params.id;
  db.query("DELETE FROM allergy WHERE allergy_id=?", id, (err, result) => {
    if (err) {
      return res.status(400).json({ message: err });
    } else {
      console.log(`Deleted the allergy ${id} successfully.`);
      res.send(result);
    }
  });
});

export default patientAllergy;
