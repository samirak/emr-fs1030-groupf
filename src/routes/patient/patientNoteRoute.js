import express from "express";
import db from "../../../server";

const patientNote = express.Router();

// Getting all medical history records of all patients
patientNote.get("/", (req, res) => {
  try {
    db.query("SELECT * FROM patient_notes", (err, result) => {
      if (err) {
        console.log(err);
        throw err;
      } else {
        console.log(result);
        res.status(200).json({ result });
        console.log(
          "Retrieved note data of all patients successfully."
        );
      }
    });
  } catch (err) {
    throw err;
  }
});

// Getting all medical records of a patient by health card as an id
patientNote.get("/:id", (req, res) => {
  try {
    const id = req.params.id;
    console.log(id);

    db.query(
      
      `SELECT * FROM patient_notes WHERE patient_healthcard =  '${id}' ORDER BY date_created DESC`,
    
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({ result });
          console.log(
            `Retrieved medical notes data of patient with healthcard number ${id} successfully.`
          );
        }
      }
    );
  } catch (err) {
    console.error("Error in getting medical notes record route!", err);
    //return next(err);
  }
});

// Adding a new medical Patient Note to db
patientNote.post("/create", (req, res) => {
    console.log(req.body.patient_healthcard);
    console.log(req.body.patientNote);
    console.log(req.body.date_created);



  try {
    let sqlQuery =
      "INSERT INTO patient_notes (patient_healthcard, patient_note, date_created) VALUES (?,?,?)";



    db.query(
      sqlQuery,
      [
        req.body.patient_healthcard,
        req.body.patient_note,
        req.body.date_created,       
      ],
      (err, result) => {
        if (err) {
          console.log(err);
          return res.status(400).json({ message: err });
        } else {
          console.log("Created a new Patient notes record successfully!");
          res.status(201).json(result);
        }
      }
    );
  } catch (err) {
    console.error("Error in creating new Patient notes record route!", err);
    //return next(err);
  }
});

// Updating an existing medical history record
patientNote.put("/update/:id", (req, res) => {
  try {
    const id = req.params.id;
    let sqlQuery = `UPDATE patient_notes SET patient_healthcard=?, patient_note=?, date_created=? WHERE id='${id}'`;
    db.query(
      sqlQuery,
      [
        req.body.patient_healthcard,
        req.body.patient_note,
        req.body.date_created,
      ],
      (err, result) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log("Updated the patient note successfully!");
          res.status(200).json(result);
        }
      }
    );
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

// Deleting an existing medical history record
patientNote.delete("/delete/:id", (req, res) => {
  try {
    const id = req.params.id;

    db.query(
      `DELETE FROM patient_notes WHERE id = '${id}'`,
      (err, result) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log(result);
          res.status(200).send(result);
        }
      }
    );
  } catch (err) {
    console.error("Error in deleting medical notes record route!", err);
    //return next(err);
  }
});

export default patientNote;
