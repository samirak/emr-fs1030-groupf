import express from "express";
import db from "../../../server.js";
//import bcrypt from "bcrypt";
import { stringify } from "flatted";

const patientAdminRoute = express.Router();
//const saltRounds = 10;

//get all patients list
patientAdminRoute.get("/", async (req, res) => {
  try {
    db.query(
      "SELECT patient_healthcard, patient_firstname, patient_lastname, patient_DOB,patient_phone, patient_street,patient_city,patient_province,patient_postalcode,patient_gender,patient_email,patient_marital,patient_insurance FROM patient_profile",
      (err, data) => {
        if (err) {
          console.log(err);
          return res.status(400).json({ message: err });
        } else {
          console.log("get all patients data successfully.");
          res.status(200).json({
            data,
          });
        }
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
});

//get a patient by id
patientAdminRoute.get("/:id", (req, res) => {
  try {
    let id = req.params.id;

    db.query(
      `SELECT * FROM patient_profile WHERE patient_healthcard = '${id}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({
            result,
          });
          console.log(`get healthcard= ${id} data successfully.`);
        }
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
});

// search one doctor by his first name and last name
patientAdminRoute.post("/search", (req, res) => {
  try {
    const careprovider_firstname = req.body.careprovider_firstname;
    const careprovider_lastname = req.body.careprovider_lastname;

    db.query(
      "SELECT careprovider_id, careprovider_firstname, careprovider_lastname, careprovider_username,careprovider_avatar, is_admin FROM care_provider WHERE careprovider_firstname =? AND careprovider_lastname =?",
      [careprovider_firstname, careprovider_lastname],
      (err, data) => {
        if (err) {
          return res.status(400).json({ message: err });
        } else {
          console.log(`find the care provider!`);
          res.status(200).json(data);
        }
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
});

//Create a new  patient
patientAdminRoute.post("/create", async (req, res) => {
  try {
    const {
      patient_healthcard,
      patient_firstname,
      patient_lastname,
      patient_DOB,
      patient_phone,
      patient_street,
      patient_city,
      patient_province,
      patient_postalcode,
      patient_gender,
      patient_email,
      patient_marital,
      patient_insurance,
    } = req.body;

    let newPatient = await db.query(
      "INSERT INTO patient_profile(patient_healthcard, patient_firstname, patient_lastname, patient_DOB,patient_phone,patient_street,patient_city,patient_province,patient_postalcode,patient_gender,patient_email,patient_marital,patient_insurance) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
      [
        patient_healthcard,
        patient_firstname,
        patient_lastname,
        patient_DOB,
        patient_phone,
        patient_street,
        patient_city,
        patient_province,
        patient_postalcode,
        patient_gender,
        patient_email,
        patient_marital,
        patient_insurance,
      ]
    );

    let newP = stringify(newPatient);
    res.status(200).json(newP);

    console.log("Add a new care patient successfully!");
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

//update a patient information
patientAdminRoute.put("/update/:id", async (req, res) => {
  try {
    const {
      patient_healthcard,
      patient_firstname,
      patient_lastname,
      patient_DOB,
      patient_phone,
      patient_street,
      patient_city,
      patient_province,
      patient_postalcode,
      patient_gender,
      patient_email,
      patient_marital,
      patient_insurance,
    } = req.body;
    const id = req.params.id;

    let updatePatient = db.query(
      `UPDATE patient_profile SET patient_healthcard=?, patient_firstname=?, patient_lastname=?, patient_DOB=?, patient_phone=?,patient_street=?,  patient_city=?, patient_province=?, patient_postalcode=?, patient_gender=?, patient_email=?, patient_marital=?,patient_insurance=? WHERE patient_healthcard=?`,
      [
        patient_healthcard,
        patient_firstname,
        patient_lastname,
        patient_DOB,
        patient_phone,
        patient_street,
        patient_city,
        patient_province,
        patient_postalcode,
        patient_gender,
        patient_email,
        patient_marital,
        patient_insurance,
        id,
      ]
    );

    let upPatient = stringify(updatePatient);
    res.status(200).json(upPatient);
    console.log("Update a care provider successfully!");
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

// delete one patient
patientAdminRoute.delete("/delete/:id", (req, res) => {
  const id = req.params.id;
  db.query(
    "DELETE FROM patient_profile WHERE patient_healthcard=?",
    id,
    (err, result) => {
      if (err) {
        return res.status(400).json({ message: err });
      } else {
        console.log(
          `Delete provider at his/her healthcard ID=${id} item successfully.`
        );
        res.send(result);
      }
    }
  );

  
});



export default patientAdminRoute;
