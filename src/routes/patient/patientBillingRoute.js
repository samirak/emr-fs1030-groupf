import express from "express";
import db from "../../../server.js";
import { stringify } from "flatted";

const patientBillingRoute = express.Router();

patientBillingRoute.get("/", (req, res) => {
  try {
    db.query(
      "SELECT patient_healthcard, transaction_type, amount, reference_number, status, date_completed FROM patient_billing ",
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({ result });
          console.log("Retrieved billing data of all patients successfully.");
        }
      }
    );
  } catch (err) {
    throw err;
  }
});

//Get a patient's billing with healthID
patientBillingRoute.get("/:healthID", (req, res) => {
  try {
    let healthID = req.params.healthID;

    db.query(
      `SELECT * FROM patient_billing WHERE patient_healthcard = '${healthID}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({
            result,
          });
          console.log(
            `successfully retrieved billing information for patient ${healthID}.`
          );
        }
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
});

//Get a patient's billing with reference_number
patientBillingRoute.get("/record/:id", (req, res) => {
  try {
    let id = req.params.id;

    db.query(
      `SELECT * FROM patient_billing WHERE reference_number = '${id}'`,
      (err, result) => {
        if (err) {
          console.log(err);
          throw err;
        } else {
          console.log(result);
          res.status(200).json({
            result,
          });
          console.log(
            `successfully retrieved billing information for patient ${id}.`
          );
        }
      }
    );
  } catch (err) {
    console.log(err);
    throw err;
  }
});

//Create New Billing For a Patient
patientBillingRoute.post("/create", async (req, res) => {
  try {
    const {
      patient_healthcard,
      transaction_type,
      amount,
      reference_number,
      status,
      date_completed,
    } = req.body;

    let newBilling = await db.query(
      "INSERT INTO patient_billing(patient_healthcard, transaction_type, amount, reference_number, status, date_completed) VALUES (?,?,?,?,?,?)",
      [
        patient_healthcard,
        transaction_type,
        amount,
        reference_number,
        status,
        date_completed,
      ]
    );

    let newB = stringify(newBilling);
    res.status(200).json(newB);

    console.log(`Billing Created for patient`);
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

//Update Billing For a Patient

patientBillingRoute.put("/update/:id", async (req, res) => {
  try {
    const {
      transaction_type,
      amount,
      reference_number,
      status,
      date_completed,
    } = req.body;
    const id = req.params.id;

    let updatePatientBilling = db.query(
      `UPDATE patient_billing SET  transaction_type=?, amount=?, reference_number=?, status=?, date_completed=? WHERE reference_number=?`,
      [transaction_type, amount, reference_number, status, date_completed, id]
    );

    let upPatientBill = stringify(updatePatientBilling);
    res.status(200).json(upPatientBill);
    console.log(`Updated Billing for Ref: ${id}!`);
  } catch (err) {
    console.error(err.message);
    res.status(500).send({ message: err });
  }
});

//Delete Billing By Reference Number

patientBillingRoute.delete("/delete/:id", (req, res) => {
  const id = req.params.id;
  db.query(
    "DELETE FROM patient_billing WHERE reference_number=?",
    id,
    (err, result) => {
      if (err) {
        return res.status(400).json({ message: err });
      } else {
        console.log(`Deleted Billing Item with Ref Number ${id}.`);
        res.send(result);
      }
    }
  );
});

export default patientBillingRoute;
