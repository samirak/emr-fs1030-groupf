import express from "express";
import db from "../../../server";
import {
  hashPassword,
  verifyPassword,
  createToken,
} from "../../middleware/utility";
//import jwt from "express-jwt";
import jwtDecode from "jwt-decode";

// require("dotenv").config();

const userRoute = express.Router();

// User Sign Up
userRoute.post("/signup", async (req, res) => {
  try {
    const hashedPassword = await hashPassword(req.body.user_password);
    const email = req.body.user_email;
    let emailQuery = `SELECT user_email FROM user WHERE user_email = "${email}"`;
    let sqlQuery =
      "INSERT INTO user (user_firstName, user_lastName, user_email, user_password, user_role) VALUES (?,?,?,?,?)";

    db.query(emailQuery, (err, result) => {
      if (err) {
        return res.status(400).json({ message: "Email already exists" });
      }

      db.query(
        sqlQuery,
        [
          req.body.user_firstName,
          req.body.user_lastName,
          req.body.user_email,
          hashedPassword,
          req.body.user_role,
        ],
        (err, data) => {
          if (err) {
            return res.status(400).json({ message: err });
          } else {
            console.log("User created!");
            res.status(201).json(data);
          }
        }
      );
    });
  } catch (err) {
    console.error("Error in creating new allergy route!", err);
    //return next(err);
  }
});

// User Login and Authentication
userRoute.post("/authenticate", async (req, res) => {
  try {
    //const { email, password } = req.body;
    let emailPasswordQuery = `SELECT * FROM user WHERE user_email = '${req.body.user_email}'`;

    //console.log(emailPasswordQuery)
    db.query(emailPasswordQuery, (err, result) => {
      //console.log(result[0]);
      if (result[0] === undefined) {        
        return res.status(400).json({ message: "Wrong email or password." });
      }

      const foundUser = result[0];
      const passwordValid = verifyPassword(req.body.user_password, foundUser.user_password);

      if (passwordValid) {
        //const { email, password, ...rest } = foundUser;
        //const userInfo = Object.assign({}, { ...rest });
        const userInfo = foundUser;

        console.log(userInfo);

        const token = createToken(userInfo);
        const decodedToken = jwtDecode(token);
        const expiresAt = decodedToken.exp;

        res.json({
          message: "Authentication successful!",
          token,
          userInfo,
          expiresAt,
        });
      } else {
        res.status(403).json({
          message: "Wrong email or password.",
        });
      }
    });
  } catch (err) {
    console.log(err);
    return res.status(400).json({ message: "Something went wrong." });
  }
});

export default userRoute;
