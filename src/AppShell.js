import React from 'react';
import Navbar from './components/Navbar/Navbar';
import SideBar from './components/SideBar/SideBar';
import "./components/SideBar/sidebar.css"

const AppShell = ({ children }) => {
  return (
    <>   
    <div className="flex">        
        <div className="flex flex-col w-full border-l border-gray-200">
          <div className="p-4 border-b border-gray-200 bg-indigo-600">
            <Navbar />
          </div>                 
        </div>
      </div>    
      <div className="flex">
        <div className="px-3 pt-6 bg-gray-200">
          <SideBar />
        </div>
        <div className="flex flex-col w-full border-0 border-gray-200">          
          <div className="px-4 sm:px-0 py-0">
            {children}
          </div>          
        </div>
      </div>        
    </>
  );
};

export default AppShell;
